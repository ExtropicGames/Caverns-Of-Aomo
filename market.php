<?php

require_once ("includes/Authentication.php");

session_start();

Authenticator::authenticate();

include "header.php";

Database::connect();

$username = $_SESSION['username'];

$player_id = Authenticator::getPlayerID();

include 'market-header.php';

echo '<center>';

$lot_result = mysql_query("SELECT * FROM market_lots ORDER BY time_posted ASC LIMIT 30");
if (mysql_num_rows($lot_result) > 0) {
    while ($lot_row = mysql_fetch_array($lot_result)) {
        echo $lot_row['player_id'].' - <a href="market-lot-view.php?lot_id='.$lot_row['lot_id'].'">'.$lot_row['description'].'</a> - '.$lot_row['time_expire'].'<br>';
    }
} else {
    echo 'No lots on the market.<br>';
}
?>
