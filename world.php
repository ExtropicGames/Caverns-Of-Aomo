<?php
// function includes
require_once ("includes/Authentication.php");
require_once ("functions-game.php");

require_once ("functions-sessions.php");

include "views/world-view.php";

// load session/post variables

$map_x      = loadPostIntoSession('map_x', 0, 'Integer');
$map_y      = loadPostIntoSession('map_y', 0, 'Integer');
$map_z      = loadPostIntoSession('map_z', 0, 'Integer');
$target_x   = loadPostIntoSession('target_x', 0, 'Integer');
$target_y   = loadPostIntoSession('target_y', 0, 'Integer');
$width      = loadFromSession('map_width', 3);
$height     = loadFromSession('map_height', 5);
$robot_id   = loadFromPost('robot_id', null, 'Integer');
$action     = loadFromPost('action', null, 'String');
$recipe_id  = loadFromPost('recipe_id', null, 'Integer');

echo "Map - X: $map_x Y: $map_y Z: $map_z<br>";
echo "Target - X: $target_x Y: $target_y<br><hr>";

Database::connect();

$player_id = Authenticator::getPlayerID();

// fetch the map array

echo "SELECT * FROM tiles WHERE " .
                           "(x<=($map_x + $width)  OR x>=($map_x - $width)) " .
                       "AND (y<=($map_y + $height) OR y>=($map_y - $height)) " .
                       "ORDER BY x DESC, y ASC";

$result = mysql_query("SELECT * FROM tiles WHERE " .
                           "(x<=($map_x + $width)  OR x>=($map_x - $width)) " .
                       "AND (y<=($map_y + $height) OR y>=($map_y - $height)) " .
                       "ORDER BY x DESC, y ASC");

if ($result == null) {
    die("The system is down. (Error code = 5) Please contact your administrator.");
}
    
while($row = mysql_fetch_array($result)) {
    update_tile_jobs($row['tile_id']);
}

$result = mysql_query($query);
if ($result == NULL) {
    die("The system is down. (Error code = 5) Please contact your administrator.");
}
    
while($row = mysql_fetch_array($result)) {
    $map[$row['x']][$row['y']]['state'] = $row['state']; 
    $map[$row['x']][$row['y']]['ref_factory_id'] = $row['ref_factory_id'];
    $map[$row['x']][$row['y']]['tile_id'] = $row['tile_id'];
}

// check for actions submitted through post

if ($action == 'mining') {
    $robot_result = mysql_query("SELECT * FROM robots WHERE robot_id='$robot_id' AND player_id='$player_id'");
    if (mysql_num_rows($robot_result) == 1) {
        $robot_row = mysql_fetch_array($robot_result);
        // If the robot is active and not working (i.e. idle)...
        if (($robot_row['active_state'] == 'idle') && ($robot_row['job_tile'] == NULL)) {
            $parts_result = mysql_query("SELECT * FROM view_components WHERE robot_id='$robot_id' AND player_id='$player_id' AND type='arm' AND attribute1='$ARM_DIGGING'");
            // And the robot has at least one digging arm equipped, and the tile is unexcavated...
            if ((mysql_num_rows($parts_result) > 0) && ($map[$target_x][$target_y]['state'] == 'new')) {
                $job_start = time();
                $job_finish = time() + 3600;
                $query = "UPDATE robots SET job_tile=" . $map[$target_x][$target_y]['tile_id'] . ", job_start=FROM_UNIXTIME('$job_start'), job_finish=FROM_UNIXTIME('$job_finish'), job_type='mining', active_state='working' WHERE robot_id='$robot_id' AND player_id='$player_id'";
                $dig_result = mysql_query($query);
                if (!$dig_result) {
                    echo 'database error<br>';
                }
            } else {
                echo 'Error: No digging arm equipped.';
            }
        } else {
            echo 'Error: Robot not idle<br>';
        }
    } else {
        echo 'Invalid robot ID.<br>';
    }
}

if ($action == 'manufacturing') {
    // If the robot asked to do the job belongs to the player...
    $robot_result = mysql_query("SELECT * FROM robots WHERE robot_id='$robot_id' AND player_id='$player_id'");
    if (mysql_num_rows($robot_result) == 1) {
        // And the robot is idle...
        $robot_row = mysql_fetch_array($robot_result);
        if (($robot_row['active_state'] == 'idle') && ($robot_row['job_tile'] == NULL)) {
            // And the robot has a manufacturing arm equipped and is trying to work at a factory...
            $parts_result = mysql_query("SELECT * FROM view_components WHERE robot_id='$robot_id' AND player_id='$player_id' AND type='arm' AND attribute1='$ARM_MANUFACTURING'");
            if ((mysql_num_rows($parts_result) > 0) && ($map[$target_x][$target_y]['state'] == 'occupied')) {
                // And the player knows the recipe...
                $recipe_result = mysql_query("SELECT * FROM player_recipes WHERE player_id='$player_id' AND ref_recipe_id='$recipe_id'");
                if (mysql_num_rows($recipe_result) == 1) {
                    // And the player has the materials to make the recipe...
                    $has_resources = true;
                    $input_result = mysql_query("SELECT * FROM ref_recipe_input WHERE ref_recipe_id='$recipe_id'");
                    while ($input_row = mysql_fetch_array($input_result)) {
                        if ($input_row['input_material_id'] == NULL) {
                            echo 'Recipes with unspecified materials are not yet supported.';
                        } else {
                            $resource_result = mysql_query("SELECT * FROM resources WHERE player_id='$player_id' AND ref_resource_id='".$input_row['input_resource_id']."' AND ref_material_id='".$input_row['input_material_id']."'");
                            $resource_row = mysql_fetch_array($resource_result);
                            if ($resource_row['quantity'] < $input_row['input_quantity']) {
                                $has_resources = false;
                                $resource_info_result = mysql_query("SELECT * FROM ref_resource WHERE ref_resource_id='".$input_row['input_resource_id']."'");
                                $resource_info_row = mysql_fetch_array($resource_info_result);
                                $material_info_result = mysql_query("SELECT * FROM ref_material WHERE ref_material_id='".$input_row['input_material_id']."'");
                                $material_info_row = mysql_fetch_array($material_info_result);
                                echo 'Requires '.$input_row['input_quantity'].' '.$material_info_row['name'].' '.$resource_info_row['name'].'.<br>';
                            }
                        }
                    }

                    if ($has_resources == true) {
                        // Then start the job!
                        $job_start = time();
                        $job_finish = time() + 3600;
                        $job_result = mysql_query("UPDATE robots SET job_tile=" . $map[$target_x][$target_y]['tile_id'] . ", job_start=FROM_UNIXTIME('$job_start'), job_finish=FROM_UNIXTIME('$job_finish'), job_type='manufacturing', active_state='working', job_recipe='$recipe_id' WHERE robot_id='$robot_id' AND player_id='$player_id'");
                        $input_result = mysql_query("SELECT * FROM ref_recipe_input WHERE ref_recipe_id='$recipe_id'");
                        while ($input_row = mysql_fetch_array($input_result)) {
                            remove_resources($player_id, $input_row['input_resource_id'], $input_row['input_material_id'], $input_row['input_quantity']);
                        }

                        if (!$job_result) {
                            echo 'database error<br>';
                        }
                    }
                } else {
                    echo 'Recipe permissions error.<br>';
                }
            } else {
                echo 'Parts/factory error.<br>';
            }
        } else {
            echo 'Robot not idle.<br>';
        }
    } else {
        echo 'You must select a robot.<br>';
    }
}

// print the map

echo '<center>';
echo '<form name="world" action="world.php" method=post>';
echo "<input type=hidden name=map_x value=$map_x><input type=hidden name=map_y value=$map_y><input type=hidden name=map_z value=$map_z>";
echo "<input type=hidden name=target_x value=$target_x><input type=hidden name=target_y value=$target_y>";
echo '<table>';
for ($i = ($map_y + $height); $i >= ($map_y - $height); $i--) {
    echo '<tr>';
    for ($m = ($map_x - $width); $m <= ($map_x + $width); $m++) {
        if (($m == $target_x) && ($i == $target_y)) {
            echo "<td border=2><a href='javascript:;' onClick='view($m, $i)'>";
        } else {
            echo "<td><a href='javascript:;' onClick='view($m, $i)'>";
        }
        switch ($map[$m][$i]['state']) {
        case 'new':
            echo "<img src='images/rock.png'>";
            break;
        case 'occupied':
            echo "<img src='images/city.png'>";
            break;
        case 'empty':
            echo "<img src='images/empty.png'>";
            break;
        default:
            echo '<img src="images/unexplored.png">';
            break;
        }
        echo '</a></td>';
    }
    echo '</tr>';
}
echo '</table>';
echo "<a href='javascript:;' onClick='move($map_x, $map_y+1, 0)'>Up</a> | ";
echo "<a href='javascript:;' onClick='move($map_x, $map_y-1, 0)'>Down</a> | ";
echo "<a href='javascript:;' onClick='move($map_x-1, $map_y, 0)'>Left</a> | ";
echo "<a href='javascript:;' onClick='move($map_x+1, $map_y, 0)'>Right</a><br>";
echo '</form>';

$result = mysql_query("SELECT * FROM view_tiles WHERE x=$target_x AND y=$target_y");
if ($result == NULL) {
    die ("This is an unoccupied tile.<br>");
}
$row = mysql_fetch_array($result);
switch ($row['state']) {
    case 'new':
        echo "This is an unmined vein of " . $row['material_name'] . ". There is " . $row['substrate_remaining'] . "kg of material left in the vein.<br><br>";
        
        echo "<form name='mining' action='world.php' method=post>";
        echo "<input type=hidden name=robot_id><input type=hidden name=action>";
        echo "<table class='spreadsheet'><tr>";
        echo "<td class='spreadsheet'>Idle Robots</td></tr>";
        // Select all idle robots with digging arms equipped
        $idle_robots = get_idle_robots($player_id, "type='arm' AND attribute1=" . ArmTypes::DIGGING);
        if ($idle_robots == NULL) {
            echo "<tr><td class='spreadsheet'>No idle robots available.</td></tr>";
        } else {
            foreach ($idle_robots as $robot) {
                echo "<tr><td class='spreadsheet'> Set <a href='javascript:;' onClick='doAction(1, ".$robot['robot_id'].")'>".$robot['name']."</a> to mine this vein</td></tr>";
            }
        }
        echo "</table><table class='spreadsheet'><tr><td class='spreadsheet'>Currently Mining</td></tr>";
        // Select all robots owned by the player that are currently working on this tile.
        $miner_result = mysql_query("SELECT * FROM robots WHERE job_tile=" . $row['tile_id'] . " AND player_id='$player_id'");
        if (mysql_num_rows($miner_result) == 0) {
            echo '<tr><td class="spreadsheet">No robots assigned to this vein.</td></tr>';
        } else {
            while ($miner_row = mysql_fetch_array($miner_result)) {
                $finish_time = strtotime($miner_row['job_finish']);
                $start_time = strtotime($miner_row['job_start']);
                $current_time = time();
                $seconds_elapsed = $current_time - $start_time;
                $seconds_total = $finish_time - $start_time;
                $percent_complete = floor(($seconds_elapsed / $seconds_total) * 100);
                //$time = update_robot_jobs($miner_row['robot_id']);
                echo "<tr><td class='spreadsheet'>" . $miner_row['name'] . " is currently mining here.<br>";
                echo "He is $percent_complete% complete.<br>";
                echo "</td></tr>";
            }
        }
        echo "</table>";
        break;
    case 'excavating':
        echo "This is a vein of " . $row['material_name'] . '. It is being mined by: no one.<br>';
        break;
    case 'empty':
        echo "This is an empty area. The walls are hewn from " . $row['material_name'] . '.<br>';
        echo "<form name='constructing' action='world.php' method=post>";
        echo '<table class="spreadsheet"><tr>';
        echo '<td class="spreadsheet">Idle Robots</td></tr>';
        $idle_robots = get_idle_robots($player_id, "type='arm' AND attribute1=" . ArmTypes::CONSTRUCTING);
        if ($idle_robots == NULL) {
            echo '<tr><td class="spreadsheet">No idle robots available.</td></tr>';
        } else {
            foreach ($idle_robots as $robot) {
                echo "<tr><td class='spreadsheet'><input type=radio name=robot_id value='".$robot['robot_id']."'>".$robot['name']."</td></tr>";
            }
        }
        echo "</table><table class='spreadsheet'><tr><td class='spreadsheet'>Factory Types</td></tr>";
        $factory_result = mysql_query("SELECT * FROM ref_factory");
        while ($factory_row = mysql_fetch_array($factory_result)) {
            echo '<tr><td class="spreadsheet"><input type=radio name=factory_id value="'.$factory_row['ref_factory_id'].'">'.$factory_row['name'].'</td></tr>';
        }
        echo '</table>';
        echo '<input type=submit value="Construct Factory">';
        echo '<br><br>';
        break;
    case 'constructing':
        echo 'This is a ' . $row['factory_name'] . ' owned by ' . $row['username'] . '. It is under construction.<br>';
        break;
// The tile is a factory
    case 'occupied':
        echo 'This is a ' . $row['factory_name'] . ' owned by ' . $row['username'] . '.<br>';
        // The tile is owned by the current player
        if ($row['owner_id'] == $player_id) {
            if ($row['factory_type'] == 'manufacturing') {
                echo "<form name='manufacturing' action='world.php' method=post>";
                echo "<input type=hidden name=action value='manufacturing'>";
                echo "<table class='spreadsheet'><tr>";
                echo "<td class='spreadsheet'>Idle Robots</td></tr>";
                $idle_robots = get_idle_robots($player_id, "type='arm' AND attribute1=" . ArmTypes::MANUFACTURING);
                if ($idle_robots == NULL) {
                    echo "<tr><td class='spreadsheet'>No idle robots available.</td></tr>";
                } else {
                    foreach ($idle_robots as $robot) {
                        echo "<tr><td class='spreadsheet'><input type=radio name=robot_id value='".$robot['robot_id']."'>".$robot['name']."</td></tr>";
                    }
                }
                echo "</table><table class='spreadsheet'><tr><td class='spreadsheet'>Known Recipes</td></tr>";
                $recipe_result = mysql_query("SELECT * FROM view_player_recipes WHERE ref_factory_id='".$row['ref_factory_id']."' AND player_id='$player_id'");
                if (mysql_num_rows($recipe_result) == 0) {
                    echo "<tr><td class='spreadsheet'>No recipes known for this factory type.</td></tr>";
                } else {
                    while ($recipe_row = mysql_fetch_array($recipe_result)) {
                        echo '<tr><td class="spreadsheet"><input type=radio name=recipe_id value="'.$recipe_row['ref_recipe_id'].'"><a href="recipes-detail.php?recipe_id='.$recipe_row['ref_recipe_id'].'">'.$recipe_row['description'].'</td></tr>';
                    }
                }
                echo '</table>';
                echo '<input type=submit value="Manufacture">';
                echo '<br><br>';
                echo "<table class='spreadsheet'><tr><td class='spreadsheet'>Currently Working</td></tr>";
                // Select all robots owned by the player that are currently working on this tile.
                $worker_result = mysql_query("SELECT * FROM robots WHERE job_tile=" . $row['tile_id'] . " AND player_id='$player_id'");
                if (mysql_num_rows($worker_result) == 0) {
                    echo '<tr><td class="spreadsheet">No robots currently assigned to this factory.</td></tr>';
                } else {
                    while ($worker_row = mysql_fetch_array($worker_result)) {
                        $finish_time = strtotime($worker_row['job_finish']);
                        $start_time = strtotime($worker_row['job_start']);
                        $current_time = time();
                        $seconds_elapsed = $current_time - $start_time;
                        $seconds_total = $finish_time - $start_time;
                        $percent_complete = floor(($seconds_elapsed / $seconds_total) * 100);
                        echo "<tr><td class='spreadsheet'>" . $worker_row['name'] . " is currently working here.<br>";
                        echo "He is $percent_complete% complete.<br>";
                        echo "</td></tr>";
                    }
                }
                echo '</form>';
            }
            if ($row['factory_type'] == 'researching') {
                echo "<form name='researching' action='world.php' method=post>";
                echo "<input type=hidden name=action value='researching'>";
                echo "<table class='spreadsheet'><tr><td class='spreadsheet'>Recipes to Research</td></tr>";
                $recipe_result = mysql_query("SELECT * FROM ref_recipe_process WHERE ref_recipe_id NOT IN (SELECT ref_recipe_id FROM player_recipes WHERE player_id = $PLAYER_ID)");
                while ($recipe_row = mysql_fetch_array($recipe_result)) {
                    echo '<tr><td class="spreadsheet"><input type=radio name=recipe_id value="'.$recipe_row['ref_recipe_id'].'">'.$recipe_row['description'].'</td></tr>';
                }
                echo '<input type=submit value="Construct Factory">';
                echo '<br><br>';
                echo '</table></form>';
            }
        }
        break;
    case 'destructing':
        echo 'This tile is exploding.<br>';
        break;
    default:
        echo "This is an unoccupied tile.<br>";
        break;
}
    
?>
</center>