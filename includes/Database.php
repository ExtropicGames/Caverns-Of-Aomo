<?php

class Database {

    //! \todo Load these from a configuration file.
    const ServerName = 'localhost';
    const Username = 'iometi5_caverns';
    const Password = 'Ni&z8Zd,-Y1z';
    const DBName = 'iometi5_caverns';

    private $mysqli = NULL;

    static function getConnection()  {
        if (isset($mysqli)) {
            return $mysqli;
        }

        $mysqli = new mysqli(Database::ServerName, Database::Username, Database::Password, Database::DBName);

        if (mysqli_connect_errno()) {
            echo 'Unable to connect to the database server.';
            die();
        }

        return $mysqli;
    }
}