<?php

require_once ('Database.php');

class Authenticator {

    /**
     * @return bool - True if the user is logged in.
     */
    static function authenticated() {
        if (isset($_SESSION['authorized']) && isset($_SESSION['username'])) {
            if ($_SESSION['ip'] == $_SERVER['REMOTE_ADDR']) {
                $mysqli = Database::getConnection();
                $time = time();
                $username = $_SESSION['username'];
                $query = "UPDATE players SET last_verified=FROM_UNIXTIME('$time') WHERE username='$username' LIMIT 1";
                $mysqli->query($query);
                return $_SESSION['authorized'];
            } else {
                // ip address changed
                return false;
            }
        } else {
            // no authorized session data
            return false;
        }
    }

    static function authenticate() {
        if (!Authenticator::authenticated()) {
            //! \todo Allow the user to log in.
            Authenticator::logout();
        }
    }

    /**
     * @return bool - True if player is an administrator.
     */
    static function isAdmin() {
        if (isset($_SESSION['player_id']) && $_SESSION['player_id'] == 1) {
            return true;
        }
        return false;
    }

    /**
     * @return int - The ID of the currently logged in player. If no player is
     *               logged in, returns 0.
     */
    static function getPlayerID() {
        if (isset($_SESSION['player_id'])) {
            return $_SESSION['player_id'];
        }
        return 0;
    }

    static function logout() {
        if (session_id() != '') {
            session_destroy();
            echo '<meta http-equiv=refresh content="0;URL=login.php">';
            die();
        }
    }
}