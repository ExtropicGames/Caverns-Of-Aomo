<?php

require_once ("includes/Authentication.php");

session_start();

Authenticator::authenticate();

include "header.php";

Database::connect();

$username = $_SESSION['username'];

$player_id = Authenticator::getPlayerID();

echo "<center><a href='market.php'>View Lots</a> - <a href='market-offers.php'>View offers on my lots</a> - <a href='market-create.php'>Create new lot</a><br><br>";

echo 'There are no offers on your lots because no one likes you.';
?>