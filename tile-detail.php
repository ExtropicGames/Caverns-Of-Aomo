<?php

require_once ("includes/Authentication.php");

session_start();

if (!Authenticator::authenticated()) {
    Authenticator::logout();
} else {
    include "header.php";
}

echo '<center>';

Database::connect();

$player_id = Authenticator::getPlayerID();

if (isset($_POST['x'])) {
    $_SESSION['map_x'] = (int)mysql_real_escape_string($_POST['x']);
}
if (isset($_POST['y'])) {
    $_SESSION['map_y'] = (int)mysql_real_escape_string($_POST['y']);
}
if (isset($_POST['z'])) {
    $_SESSION['map_z'] = (int)mysql_real_escape_string($_POST['z']);
}
   
$x = $_SESSION['map_x'];
$y = $_SESSION['map_y'];
$z = $_SESSION['map_z'];

$query = "SELECT * FROM tiles WHERE x=$x AND y=$y AND z=$z";
$result = mysql_query($query);
if ($result == NULL) {
    die("Nothing exists at that location.");
}

$row = mysql_fetch_array($result);

if ($row['state'] == 'new') {
    $query = "SELECT * FROM ref_substrate WHERE ref_substrate_id='" . $row['ref_substrate_id'] . "'";
    $i = mysql_fetch_array(mysql_query($query));
    echo "This is an outcropping of " . $i['name'] . ".<br>";
    echo $i['description'] . "<br>";
    echo $row['substrate_remaining'] . "kg of material is remaining.<br>";
}

if ($row['state'] == 'empty') {
    $query = "SELECT * FROM ref_substrate WHERE ref_substrate_id='" . $row['ref_substrate_id'] . "'";
    $i = mysql_fetch_array(mysql_query($query));
    echo "This is an empty room. The walls and floors are hewn from " . $i['name'] . ".<br>";
}

if ($row['state'] == 'occupied') {
    $query = "SELECT * FROM ref_factory WHERE ref_factory_id='" . $row['ref_factory_id'] . "'";
    $i = mysql_fetch_array(mysql_query($query));
    echo "This is a " . $i['name'] . ".<br>";
    echo $i['description'] . "<br>";
}
?>