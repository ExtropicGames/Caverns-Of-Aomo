<?php
require_once ("config.php");
require_once ("includes/Database.php");
// error code 3 = could not get result from mysql database

    $valid = true;
    Database::connect();
    $username = mysql_real_escape_string( $_POST['username']);
    $password = mysql_real_escape_string( $_POST['password']);
    $passwordconfirm = mysql_real_escape_string( $_POST['passwordconfirm']);
    $firstname = mysql_real_escape_string( $_POST['firstname']);
    $lastname = mysql_real_escape_string( $_POST['lastname']);
    $email = mysql_real_escape_string( $_POST['email']);    
    $gender = 0;
    if ($_POST['gender'] == "Male") {
        $gender = 1;
    } else if ($_POST['gender'] == "Female") {
        $gender = 2;
    }
    $month = (int)$_POST['month'];
    $day = (int)$_POST['day'];
    $year = (int)$_POST['year'];
    
    if ($month < 1 || $month > 12  || $day < 1 || $day > 31 || $year < 1900 || $year > 2100) {
        echo "Invalid birthdate. Please try again.<br>";
        $valid = false;
    } else {
        $birthday = mktime(0,0,0,$month,$day,$year);
    }
    
    if (strlen($username) < 3) {
        echo "Username is too short. Please make your username at least 3 characters long.<br>";
        $valid = false;
    }

    if (strlen($password) < 5) {
        echo "Password is too short. Please make your password at least 5 characters long.<br>";
        $valid = false;
    }

    if ($password != $passwordconfirm) {
        echo "Passwords do not match.<br>";
        $valid = false;
    }
    
    // Things to add:
    // Compare birthday with server time. Player must be at least 13 years old.
    // Email address must be valid. Send an email with verification code.
    // Make sure no string is too long.
    // When going back, fill the forms back in. Also make sure the EULA is agreed to automatically.

    if ($valid == false) {
        echo "<a href='register.php'>Back</a>";
        die();
    }
    
    $query = "SELECT username FROM players WHERE username='$username'";
    $result = mysql_query($query);
    if (mysql_num_rows($result) > 0) {
        echo "Sorry, but that username has already been taken. Please try a different username.<br>";
        echo "<a href='register.php'>Back</a>";
        die();
    }

    $password = md5($password);
    $signup = mktime();
    $last_login = mktime();
    $last_action = mktime();
    $last_verified = mktime();

    $query = "INSERT INTO players (username, password, first_name, last_name, email, gender, birthday, signup) " .
             "VALUES ('$username', '$password', '$firstname', '$lastname', '$email', '$gender', FROM_UNIXTIME('$birthday'), FROM_UNIXTIME('$signup'))";
    if (!($result = mysql_query($query)))  {
        die("The system is down. (Error code = 4) Please contact your administrator.");
    }
    $player_result = mysql_query("SELECT * FROM players WHERE username='$username'");
    $player_row = mysql_fetch_array($player_result);
    $player_id=$player_row['player_id'];
    $botname = $username . 'Bot';
    mysql_query("INSERT INTO robots (player_id, name, active_state) VALUES ('$player_id', '$botname', 'inactive')");
    mysql_query("INSERT INTO components (player_id, ref_component_id, created) VALUES ('$player_id', 2, NOW())");
    mysql_query("INSERT INTO components (player_id, ref_component_id, created) VALUES ('$player_id', 4, NOW())");
    mysql_query("INSERT INTO components (player_id, ref_component_id, created) VALUES ('$player_id', 5, NOW())");
    mysql_query("INSERT INTO components (player_id, ref_component_id, created) VALUES ('$player_id', 6, NOW())");
    mysql_query("INSERT INTO components (player_id, ref_component_id, created) VALUES ('$player_id', 8, NOW())");
    mysql_query("INSERT INTO components (player_id, ref_component_id, created) VALUES ('$player_id', 9, NOW())");
    echo "You are now registered! <a href='login.php'>Click here</a> to log in and start playing.";
    /*
    if (mysql_num_rows($result) == 1)  {
        if(session_id() == "")  { session_start(); }
        $_SESSION['username'] = "$username";
        $_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
        $_SESSION['authorized'] = true;
        echo "Logging in...";
        echo '<meta http-equiv=refresh content="0;URL=index.php">';
    } else {
        echo "Incorrect username/password. Please return to the login screen and try again.<br>";
        echo "<a href='register.php'>Back</a>";
    }
    */
?>
