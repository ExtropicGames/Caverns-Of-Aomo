<?php

require_once ("includes/Authentication.php");

session_start();

Authenticator::authenticate();

include "header.php";

Database::connect();

$username = $_SESSION['username'];

$player_id = Authenticator::getPlayerID();

echo "<center><a href='market.php'>View Lots</a> - <a href='market-offers.php'>View offers on my lots</a> - <a href='market-create.php'>Create new lot</a><br><br>";

echo '<form action="market-lot-create-action.php" method=post>';

echo 'Description of lot:<br><textarea name=description cols=30 rows=10></textarea><br>';

// Resources list
echo '<br><br>Choose items to place in the lot:<br><br>';
$query = "SELECT * FROM view_resources WHERE player_id='$player_id'";
$result = mysql_query($query);
if ($result != NULL) {
    $x = 0;
    $index = "";
    echo '<table>';
    echo '<tr>';
    while($row = mysql_fetch_array($result)) {
        echo '<td>';
        echo '<input type=text size=1 name='.$row['ref_material_id'].'-'.$row['ref_resource_id'].' value=0>/'.$row['quantity'];
        $index = $index . $row['ref_material_id'] . '-' . $row['ref_resource_id'] . ':';
        echo '  <img src="images/resources/' . $row['material_name'].'-'.$row['resource_name'].'.png">  '.$row['material_name'] . ' ' . $row['resource_name'];
        echo '</td>';
        $x++;
        if ($x > $ROWSIZE) {
            echo '</tr><tr>';
            $x = 0;
        }
    }
    echo "<input type=hidden name='index' value='$index'>";
    echo '</table>';
} else {
    echo 'You have no resources to offer on the market.';
}
echo "<input type=submit value='Create Lot'>";
echo '</form>';
?>