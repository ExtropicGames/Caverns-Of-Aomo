<?php

require_once ("includes/Authentication.php");

session_start();

Authenticator::authenticate();

if (!Authenticator::isAdmin()) {
    die('<meta http-equiv=refresh content="0;URL=index.php">');
}

include "views/header.php";

?>

<script type="text/javascript">
<!--
function move(x, y, z) {
    document.map.move_x.value = x; 
    document.map.move_y.value = y; 
    document.map.move_z.value = z;
    document.map.submit();
}
//-->
</script>

<?php

if (!isset($_SESSION['worldmap_x'])) {
    $_SESSION['worldmap_x'] = 0;
}
if (!isset($_SESSION['worldmap_y'])) {
    $_SESSION['worldmap_y'] = 0;
}
if (!isset($_SESSION['worldmap_z'])) {
    $_SESSION['worldmap_z'] = 0;
}

$x = $_SESSION['worldmap_x'];
$y = $_SESSION['worldmap_y'];
$z = $_SESSION['worldmap_z'];
    
if (isset($_POST['move_x'])) {
    $x = (int)mysql_real_escape_string($_POST['move_x']); 
    $_SESSION['worldmap_x'] = $x;
}

if (isset($_POST['move_y'])) {
    $y = (int)mysql_real_escape_string($_POST['move_y']); 
    $_SESSION['worldmap_y'] = $y;
}

if (isset($_POST['move_z'])) {
    $z = (int)mysql_real_escape_string($_POST['move_z']); 
    $_SESSION['worldmap_z'] = $z;
}

$mysqli = Database::getConnection();

$query = "SELECT * FROM tiles WHERE map_id=1 AND (x=$x OR x=$x + 1 OR x=$x - 1) AND (y=$y OR y=$y+1 OR y=$y-1) ORDER BY x DESC, y ASC";
$result = $mysqli->query($query);
if ($result == NULL) {
    die("The system is down. (Error code = 5) Please contact your administrator.");
}
    
while($row = $result->fetch_array()) {
    $map[$row['x']][$row['y']]['state'] = $row['state'];
    $map[$row['x']][$row['y']]['difficulty'] = $row['difficulty'];
    $map[$row['x']][$row['y']]['level'] = $row['level'];
    $map[$row['x']][$row['y']]['contents'] = $row['contents'];
    $map[$row['x']][$row['y']]['start_time'] = $row['start_time'];
    $map[$row['x']][$row['y']]['duration'] = $row['duration'];
}

echo '<center><form name="map" action="admin-world.php" method=post><input type=hidden name=move_x><input type=hidden name=move_y><input type=hidden name=move_z><table>';
for ($i = ($y + 1); $i >= ($y - 1); $i--) {
    echo '<tr>';
    for ($m = ($x - 1); $m <= ($x + 1); $m++) {
        echo '<td>';
        if ($map[$m][$i]['state'] == 'new') {
            echo "<a href='javascript:;' onClick='move($m, $i, 0)'><img src='images/rock.png'></a>";
        } else if ($map[$m][$i]['state'] == 'occupied') {
            echo "<a href='javascript:;' onClick='move($m, $i, 0)'><img src='images/city.png'></a>";
        } else {
            echo '<img src="images/unexplored.png">';
        }
        echo '</td>';
    }
    echo '</tr>';
}
echo '</table>';
    
?>

Hey look, a city!</center>