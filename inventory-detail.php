<?php

require_once ("includes/Authentication.php");

session_start();

Authenticator::authenticate();

include "header.php";

echo '<link rel="stylesheet" href="style/tooltip.css">';
echo '<script type="text/javascript" src="script/tooltip.js"></script>';

$ROWSIZE = 3;

Database::connect();

$username = $_SESSION['username'];

$player_id = Authenticator::getPlayerID();

$resource_id = mysql_real_escape_string($_GET['resource']);
$material_id = mysql_real_escape_string($_GET['material']);

// Resources list
echo '<center>Resources:<br><br><br><br>';
$query = "SELECT * FROM view_resources WHERE player_id='$player_id' AND ref_resource_id='$resource_id' ANd ref_material_id='$material_id'";
$result = mysql_query($query);
if ($result != NULL) {
    $x = 0;
    echo '<table>';
    echo '<tr>';
    while($row = mysql_fetch_array($result)) {
        echo '<td>';
        echo '<img onmouseover="tooltip.show(\'<u>'  . $row['material_name'] . ' ' . $row['resource_name'] . '</u><br>' . $row['description'] .  '\');" onmouseout="tooltip.hide();" src="images/resources/' . $row['material_name'].'-'.$row['resource_name'].'.png">';
        echo '<br>' . $row['quantity'] . 'x';
        echo '</td>';
        $x++;
        if ($x > $ROWSIZE) {
            echo '</tr><tr>';
            $x = 0;
        }
    }
    echo '</table><br><br>';
    echo '<form action=inventory-give.php method=post>';
    echo "<input type=hidden name=resource_id value='$resource_id'>";
    echo "<input type=hidden name=material_id value='$material_id'>";
    echo 'Give this item to another player:<br>';
    echo 'Quantity to give: <input type=text name=quantity>';
    echo 'Name of player: <input type=text name=target_player>';
    echo '<input type=submit value="Give">';
    echo '</form>';
} else {
    echo 'Invalid resource id.';
}
?>