<?php
	include ("config.php");

	if (session_id() == "") {
        session_start();
    }
?>

<html>
    <head>
        <link rel=stylesheet type=text/css href=<?php echo SystemInfo::StylesheetURL ?>>
        <title><?php echo SystemInfo::PageTitle; echo SystemInfo::Version; ?></title>
    </head>
    <body>
		<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
		
		<h3>Register for a new account</h3>
        <center>
            I agree not to sue anyone ever.<br><br>
            <form action="register.php" method=post>
                <input type=submit name=agree value="I agree.">
                <input type=submit name=disagree value="I do not agree.">
            </form>
        </center>
	</body>
</html>