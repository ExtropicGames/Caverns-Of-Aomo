<?php

require_once ("includes/Authentication.php");
include('models/Player.php');

session_start();

Authenticator::authenticate();

$mysqli = Database::getConnection();

$player = Player::getPlayerByID(Authenticator::getPlayerID());

include "views/header.php";

echo '<center>';

$result = $mysqli->query('SELECT * FROM robots WHERE player_id=' . $player->getPlayerID());
if ($result->num_rows > 0) {
    while($robot_row = $result->fetch_array()) {
        echo '<a href=robots-detail.php?robot-id=' . $robot_row['robot_id'] . '>' .  $robot_row['name'] . '</a>' . ': ';
        switch($robot_row['active_state']) {
        case 'working':
            $finish_time = strtotime($robot_row['job_finish']);
            $start_time = strtotime($robot_row['job_start']);
            $current_time = time();
            $seconds_elapsed = $current_time - $start_time;
            $seconds_total = $finish_time - $start_time;
            $percent_complete = floor(($seconds_elapsed / $seconds_total) * 100);
            echo "<font color=green><b>WORKING ($percent_complete% done)</b></font><br>";
            break;
        case 'idle':
            echo '<font color=yellow><b>IDLE</b></font><br>';
            break;
        case 'inactive':
            echo '<font color=red><b>INACTIVE</b></font><br>';
            break;
        default:
            echo '<font color=black><b>ERROR</b></font><br>';
        }
    }
} else {
    echo "You have no assembled robots.<br>";
}
    
$query = 'SELECT * FROM view_components WHERE player_id=' . $player->getPlayerID() . ' AND isnull(robot_id)';
$result = $mysqli->query($query);
if ($result != NULL) {
    echo "<hr>Unattached components:<br>";
    while($row = $result->fetch_array()) {
        echo $row['name'] . '<br>';
    }
}

echo '</center>';
?>