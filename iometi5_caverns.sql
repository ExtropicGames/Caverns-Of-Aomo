-- phpMyAdmin SQL Dump
-- version 3.5.2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 05, 2013 at 10:00 PM
-- Server version: 5.5.29
-- PHP Version: 5.4.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `iometi5_caverns`
--

-- --------------------------------------------------------

--
-- Table structure for table `chat_buffer`
--

DROP TABLE IF EXISTS `chat_buffer`;
CREATE TABLE IF NOT EXISTS `chat_buffer` (
  `ref_chat_id` int(4) NOT NULL AUTO_INCREMENT,
  `player_id` int(4) NOT NULL,
  `text` mediumtext NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ref_chat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `components`
--

DROP TABLE IF EXISTS `components`;
CREATE TABLE IF NOT EXISTS `components` (
  `component_id` int(4) NOT NULL AUTO_INCREMENT COMMENT 'unique component instance identifier',
  `player_id` int(4) NOT NULL,
  `ref_component_id` int(4) NOT NULL,
  `damage_taken` float NOT NULL COMMENT 'Ashes to ashes, dust to dust',
  `dyn_attribute` int(4) NOT NULL COMMENT 'Arms: ammo / Legs: Endurance / Head: Jamming / Power Supply: Energy remaining',
  `created` datetime NOT NULL,
  `robot_id` int(4) DEFAULT NULL,
  PRIMARY KEY (`component_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='All instantiated components reside in this table' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `maps`
--

DROP TABLE IF EXISTS `maps`;
CREATE TABLE IF NOT EXISTS `maps` (
  `map_id` int(4) NOT NULL AUTO_INCREMENT COMMENT 'link to tiles',
  `name` varchar(32) DEFAULT NULL,
  `player_id` int(4) DEFAULT NULL COMMENT 'link to player',
  `city_id` int(4) DEFAULT NULL COMMENT 'link to city',
  PRIMARY KEY (`map_id`),
  KEY `player_id` (`player_id`),
  KEY `city_id` (`city_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `maps`
--

INSERT INTO `maps` (`map_id`, `name`, `player_id`, `city_id`) VALUES
(1, 'World Map', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

DROP TABLE IF EXISTS `players`;
CREATE TABLE IF NOT EXISTS `players` (
  `player_id` int(4) NOT NULL AUTO_INCREMENT,
  `map_id` int(4) DEFAULT NULL,
  `username` varchar(32) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `first_name` varchar(32) DEFAULT NULL,
  `last_name` varchar(32) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `avatar_url` varchar(128) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `signup` datetime DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `signature` varchar(512) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_action` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_verified` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `settings_chatbox_length` int(11) NOT NULL,
  `timezone` int(4) NOT NULL,
  PRIMARY KEY (`player_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `players`
--

INSERT INTO `players` (`player_id`, `map_id`, `username`, `password`, `first_name`, `last_name`, `email`, `avatar_url`, `gender`, `birthday`, `signup`, `url`, `signature`, `last_login`, `last_action`, `last_verified`, `settings_chatbox_length`, `timezone`) VALUES
(2, 2, 'test', '098f6bcd4621d373cade4e832627b4f6', 'Jane', 'Doe', 'janedoe@gmail.com', NULL, 'female', '1988-10-09', '2009-04-25 17:39:21', NULL, NULL, '2009-04-25 17:39:21', '2009-04-26 00:39:21', '2009-08-11 03:18:06', 10, 1),
(1, 1, 'admin', '27cb602947a5fe268bcabf90cf722a04', '', '', NULL, NULL, NULL, NULL, '2009-04-25 23:12:03', NULL, NULL, NULL, '2009-04-26 04:12:16', '2009-05-21 22:07:41', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `player_recipes`
--

DROP TABLE IF EXISTS `player_recipes`;
CREATE TABLE IF NOT EXISTS `player_recipes` (
  `player_id` int(4) NOT NULL,
  `ref_recipe_id` int(4) NOT NULL,
  KEY `player_id` (`player_id`),
  KEY `ref_recipe_id` (`ref_recipe_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Relates players to zero or more recipes that they have learn';

-- --------------------------------------------------------

--
-- Table structure for table `processors`
--

DROP TABLE IF EXISTS `processors`;
CREATE TABLE IF NOT EXISTS `processors` (
  `ref_processor_id` int(4) NOT NULL,
  `processor_id` int(4) NOT NULL AUTO_INCREMENT,
  `skill_digging` int(4) NOT NULL,
  PRIMARY KEY (`processor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ref_chassis`
--

DROP TABLE IF EXISTS `ref_chassis`;
CREATE TABLE IF NOT EXISTS `ref_chassis` (
  `ref_chassis_id` int(4) NOT NULL AUTO_INCREMENT,
  `num_arms` int(4) DEFAULT NULL,
  `num_legs` int(4) DEFAULT NULL,
  `num_heads` int(4) DEFAULT NULL,
  `num_power_supplies` int(4) DEFAULT NULL,
  PRIMARY KEY (`ref_chassis_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ref_chassis`
--

INSERT INTO `ref_chassis` (`ref_chassis_id`, `num_arms`, `num_legs`, `num_heads`, `num_power_supplies`) VALUES
(1, 2, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ref_component`
--

DROP TABLE IF EXISTS `ref_component`;
CREATE TABLE IF NOT EXISTS `ref_component` (
  `ref_component_id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  `type` enum('chassis','power supply','head','arm','leg','processor') NOT NULL,
  `weight` float NOT NULL,
  `durability` int(4) NOT NULL,
  `energy_drain` float NOT NULL,
  `attribute1` int(4) NOT NULL COMMENT 'Arms: Usage type / Legs: leg type / Head: visual acuity / Power supply: fuel type / Chassis: ref_chassis_id',
  `attribute2` int(4) NOT NULL COMMENT 'Arms: Usage ability / Legs: Speed / Head: Uplink speed / Power supply: Fuel capacity / Chassis: unused',
  `attribute3` int(4) NOT NULL COMMENT 'Arms: combat skill / Legs: load capacity / Head: scanner type / Power supply: unused / Chassis: unused',
  `attribute4` int(4) NOT NULL COMMENT 'Arms: Damage type / Legs: Agility / Head: Scan power / Power supply: unused / Chassis: unused',
  `image` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`ref_component_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `ref_component`
--

INSERT INTO `ref_component` (`ref_component_id`, `name`, `type`, `weight`, `durability`, `energy_drain`, `attribute1`, `attribute2`, `attribute3`, `attribute4`, `image`) VALUES
(1, 'Corroded Copper Chassis', 'chassis', 0, 0, 0, 1, 0, 0, 0, NULL),
(2, 'Polished Brass Chassis', 'chassis', 100, 100, 15, 1, 0, 0, 0, NULL),
(3, 'Rusty Iron Chassis', 'chassis', 0, 0, 0, 1, 0, 0, 0, NULL),
(4, 'Polished Brass Manipulating Arm', 'arm', 0, 0, 0, 2, 0, 0, 0, NULL),
(5, 'Polished Brass Legs', 'leg', 0, 0, 0, 0, 0, 0, 0, NULL),
(6, 'Polished Brass Head', 'head', 0, 0, 0, 0, 0, 0, 0, NULL),
(7, 'Polished Brass Revolver Arm', 'arm', 0, 0, 0, 0, 0, 0, 0, NULL),
(8, 'Polished Brass Digging Drill', 'arm', 50, 100, 15, 1, 1, 15, 1, NULL),
(9, 'Wind-up Clockwork Engine', 'power supply', 0, 0, 0, 0, 0, 0, 0, NULL),
(10, 'Cast-Iron Stirling Engine', 'power supply', 0, 0, 0, 0, 0, 0, 0, NULL),
(11, 'Brass Analytical Processor Core', 'processor', 0, 0, 0, 0, 0, 0, 0, NULL),
(12, 'Sentient Stone Processor Core', 'processor', 0, 0, 0, 0, 0, 0, 0, NULL),
(13, 'Animated Quartz Processor Core', 'processor', 0, 0, 0, 0, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ref_factory`
--

DROP TABLE IF EXISTS `ref_factory`;
CREATE TABLE IF NOT EXISTS `ref_factory` (
  `ref_factory_id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  `description` varchar(512) NOT NULL,
  PRIMARY KEY (`ref_factory_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `ref_factory`
--

INSERT INTO `ref_factory` (`ref_factory_id`, `name`, `description`) VALUES
(1, 'smelter', 'Where ores are smelted down into ingots'),
(2, 'casting foundry', 'basic metal items are made here'),
(3, 'workshop', 'assemble parts into complete components');

-- --------------------------------------------------------

--
-- Table structure for table `ref_material`
--

DROP TABLE IF EXISTS `ref_material`;
CREATE TABLE IF NOT EXISTS `ref_material` (
  `ref_material_id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `value` int(4) DEFAULT NULL,
  `default_resource_id` int(4) NOT NULL COMMENT 'what form the material takes in its raw state',
  PRIMARY KEY (`ref_material_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `ref_material`
--

INSERT INTO `ref_material` (`ref_material_id`, `name`, `value`, `default_resource_id`) VALUES
(1, 'hematite', 0, 1),
(2, 'malachite', 0, 1),
(3, 'sphalerite', 0, 1),
(4, 'cassiterite', 0, 1),
(5, 'anthracite', 0, 1),
(6, 'iron', 0, 0),
(7, 'copper', 0, 0),
(8, 'zinc', 0, 0),
(9, 'tin', 0, 0),
(10, 'brass', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ref_processor`
--

DROP TABLE IF EXISTS `ref_processor`;
CREATE TABLE IF NOT EXISTS `ref_processor` (
  `ref_processor_id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `capacity` int(4) NOT NULL COMMENT 'storage (HD space)',
  `performance` int(4) NOT NULL COMMENT 'speed (ghz)',
  PRIMARY KEY (`ref_processor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ref_recipe_input`
--

DROP TABLE IF EXISTS `ref_recipe_input`;
CREATE TABLE IF NOT EXISTS `ref_recipe_input` (
  `ref_recipe_id` int(4) NOT NULL,
  `input_resource_id` int(4) NOT NULL,
  `input_material_id` int(4) NOT NULL DEFAULT '0' COMMENT 'if null, can be any material',
  `input_quantity` int(4) NOT NULL,
  `affected_attribute` int(4) NOT NULL,
  KEY `ref_resource_recipe_id` (`ref_recipe_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_recipe_input`
--

INSERT INTO `ref_recipe_input` (`ref_recipe_id`, `input_resource_id`, `input_material_id`, `input_quantity`, `affected_attribute`) VALUES
(2, 2, 0, 2, 0),
(7, 1, 1, 1, 0),
(1, 2, 8, 1, 0),
(1, 2, 7, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ref_recipe_output`
--

DROP TABLE IF EXISTS `ref_recipe_output`;
CREATE TABLE IF NOT EXISTS `ref_recipe_output` (
  `ref_recipe_id` int(4) NOT NULL,
  `output_component_id` int(4) DEFAULT NULL,
  `output_resource_id` int(4) DEFAULT NULL,
  `output_material_id` int(4) DEFAULT NULL COMMENT 'if null, will be same as input material',
  `output_quantity` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_recipe_output`
--

INSERT INTO `ref_recipe_output` (`ref_recipe_id`, `output_component_id`, `output_resource_id`, `output_material_id`, `output_quantity`) VALUES
(2, NULL, 3, NULL, 1),
(7, NULL, 2, 6, 1),
(1, NULL, 2, 10, 3);

-- --------------------------------------------------------

--
-- Table structure for table `ref_recipe_process`
--

DROP TABLE IF EXISTS `ref_recipe_process`;
CREATE TABLE IF NOT EXISTS `ref_recipe_process` (
  `ref_recipe_id` int(4) NOT NULL,
  `ref_factory_id` int(4) NOT NULL COMMENT 'factory needed to produce this recipe',
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`ref_recipe_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_recipe_process`
--

INSERT INTO `ref_recipe_process` (`ref_recipe_id`, `ref_factory_id`, `description`) VALUES
(1, 1, 'make brass ingots from 66% copper, 33% tin'),
(2, 2, 'makes a metal sheet from 2 ingots'),
(3, 2, 'make a pipe out of an ingot'),
(4, 2, 'make 20 bolts from an ingot'),
(5, 2, 'make 3 gears from an ingot'),
(6, 2, 'make 3 springs from ingot'),
(7, 1, 'make iron ingot from hematite ore');

-- --------------------------------------------------------

--
-- Table structure for table `ref_resource`
--

DROP TABLE IF EXISTS `ref_resource`;
CREATE TABLE IF NOT EXISTS `ref_resource` (
  `ref_resource_id` int(4) NOT NULL AUTO_INCREMENT COMMENT 'for elements, this is atomic_number',
  `name` varchar(32) DEFAULT NULL,
  `value` float DEFAULT NULL COMMENT 'value per unit',
  `icon` varchar(32) NOT NULL,
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`ref_resource_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `ref_resource`
--

INSERT INTO `ref_resource` (`ref_resource_id`, `name`, `value`, `icon`, `description`) VALUES
(1, 'ore', NULL, 'ore.png', 'Unprocessed material. Can be refined into metal ingots.'),
(2, 'ingot', NULL, 'ingot.png', 'Metal in its raw form. Used in the manufacture of all metal goods.'),
(3, 'sheet', NULL, 'sheet.png', 'metal only'),
(4, 'pipe', NULL, 'pipe.png', 'metal only'),
(5, 'bolt', NULL, 'bolt.png', 'metal only'),
(6, 'gear', NULL, 'gear.png', 'metal only'),
(7, 'spring', NULL, 'spring.png', 'metal only'),
(8, 'drill head', NULL, 'drill.png', 'Used in the manufacture of digging arms.');

-- --------------------------------------------------------

--
-- Table structure for table `ref_substrate`
--

DROP TABLE IF EXISTS `ref_substrate`;
CREATE TABLE IF NOT EXISTS `ref_substrate` (
  `ref_substrate_id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  `group` varchar(32) DEFAULT NULL,
  `icon` varchar(64) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `hardness` float DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `chemistry` varchar(128) DEFAULT NULL COMMENT 'Chemical composition',
  PRIMARY KEY (`ref_substrate_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `ref_substrate`
--

INSERT INTO `ref_substrate` (`ref_substrate_id`, `name`, `group`, `icon`, `description`, `hardness`, `url`, `chemistry`) VALUES
(1, 'Shale', NULL, NULL, 'Shale is the most commonly found sedimentary rock. It has a level of 3 on the Mohs scale of hardness.', 3, NULL, NULL),
(2, 'Granite', NULL, NULL, 'An igneous intrusive rock with a hardness of 7.5 on the Mohs scale.', 7.5, NULL, NULL),
(3, 'Limestone', NULL, NULL, 'A white sedimentary rock composed of calcite. It has a hardness of 3.', 3, NULL, NULL),
(4, 'Marble', NULL, NULL, 'A metamorphic stone that forms when limestone is placed under intense pressure. It has a hardness of 3.5.', 3.5, NULL, NULL),
(5, 'Anorthite', 'Plagioclase', NULL, 'An abundant mineral in the Earth''s and Luna''s crust. ', 8, 'http://webmineral.com/data/Anorthite.shtml', 'CaAl2Si2O8'),
(6, 'Basalt', NULL, NULL, 'A common extrusive volcanic rock. It is usually grey to black and fine-grained due to rapid cooling of lava at the surface of a planet. It may be porphyritic containing larger crystals in a fine matrix, or vesicular, or frothy scoria. Unweathered basalt is black or grey.', 2.2, 'http://en.wikipedia.org/wiki/Basalt', NULL),
(7, 'Armalcolite', NULL, NULL, 'Armalcolite is a mineral that was discovered at Tranquility Base on the Moon by the Apollo 11 crew in 1969. It was named for Armstrong, Aldrin and Collins, the three Apollo 11 astronauts.', 6, 'http://webmineral.com/data/Armalcolite.shtml', '(Mg,Fe++)Ti2O5');

-- --------------------------------------------------------

--
-- Table structure for table `ref_substrate_resource`
--

DROP TABLE IF EXISTS `ref_substrate_resource`;
CREATE TABLE IF NOT EXISTS `ref_substrate_resource` (
  `ref_substrate_id` int(4) NOT NULL,
  `ref_resource_id` int(4) NOT NULL COMMENT 'Reference to primary key of ref_resource table',
  `percent` float NOT NULL,
  `oxide` float DEFAULT NULL COMMENT 'percent of composition when in oxide form',
  `description` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ref_substrate_id`,`ref_resource_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Provides the resource composition of various substrates';

--
-- Dumping data for table `ref_substrate_resource`
--

INSERT INTO `ref_substrate_resource` (`ref_substrate_id`, `ref_resource_id`, `percent`, `oxide`, `description`) VALUES
(7, 12, 0.0877, 0.1454, 'magnesium'),
(7, 22, 0.4605, 0.7683, 'titanium'),
(7, 26, 0.0671, 0.0864, 'iron oxide'),
(7, 8, 0.3847, 0, 'O'),
(5, 11, 0.0041, 0.0056, 'Na'),
(5, 20, 0.1372, 0.192, 'Calcium Oxide (NA2O)'),
(5, 13, 0.1897, 0.3584, 'Al2O3'),
(5, 14, 0.2075, 0.444, 'SiO2'),
(5, 8, 0.4614, 0, 'Total Oxygen');

-- --------------------------------------------------------

--
-- Table structure for table `resources`
--

DROP TABLE IF EXISTS `resources`;
CREATE TABLE IF NOT EXISTS `resources` (
  `player_id` int(4) NOT NULL DEFAULT '1',
  `ref_resource_id` int(4) NOT NULL,
  `ref_material_id` int(4) NOT NULL,
  `quantity` float NOT NULL,
  UNIQUE KEY `ref_resource_id` (`ref_resource_id`,`ref_material_id`,`player_id`),
  KEY `player_id` (`player_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `robots`
--

DROP TABLE IF EXISTS `robots`;
CREATE TABLE IF NOT EXISTS `robots` (
  `robot_id` int(4) NOT NULL AUTO_INCREMENT,
  `player_id` int(4) NOT NULL,
  `name` varchar(32) NOT NULL COMMENT 'player-defined',
  `is_active` tinyint(1) NOT NULL COMMENT 'only true if robot has all necessary components and player chooses to activate it',
  `active_state` enum('inactive','idle','working') NOT NULL DEFAULT 'inactive' COMMENT 'to replace is_active',
  `job_tile` int(4) DEFAULT NULL,
  `job_start` datetime DEFAULT NULL,
  `job_finish` datetime DEFAULT NULL,
  `job_type` enum('mining','manufacturing','research') DEFAULT NULL,
  `job_factory` int(4) NOT NULL,
  `job_recipe` int(4) NOT NULL,
  PRIMARY KEY (`robot_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tiles`
--

DROP TABLE IF EXISTS `tiles`;
CREATE TABLE IF NOT EXISTS `tiles` (
  `tile_id` int(4) NOT NULL AUTO_INCREMENT,
  `owner_id` int(4) NOT NULL,
  `x` int(4) DEFAULT NULL,
  `y` int(4) DEFAULT NULL,
  `z` int(4) DEFAULT NULL,
  `state` enum('new','excavating','empty','constructing','occupied','destructing') NOT NULL DEFAULT 'new',
  `ref_material_id` int(4) DEFAULT NULL,
  `substrate_remaining` int(4) DEFAULT NULL COMMENT 'How much substrate remains to be removed',
  `ref_factory_id` int(4) DEFAULT NULL,
  `factory_level` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`tile_id`),
  UNIQUE KEY `map_id_2` (`owner_id`,`x`,`y`,`z`),
  KEY `map_id` (`owner_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_chat`
--
DROP VIEW IF EXISTS `view_chat`;
CREATE TABLE IF NOT EXISTS `view_chat` (
`ref_chat_id` int(4)
,`player_id` int(4)
,`username` varchar(32)
,`text` mediumtext
,`timestamp` timestamp
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `view_components`
--
DROP VIEW IF EXISTS `view_components`;
CREATE TABLE IF NOT EXISTS `view_components` (
`component_id` int(4)
,`player_id` int(4)
,`ref_component_id` int(4)
,`damage_taken` float
,`dyn_attribute` int(4)
,`robot_id` int(4)
,`name` varchar(32)
,`type` enum('chassis','power supply','head','arm','leg','processor')
,`weight` float
,`durability` int(4)
,`energy_drain` float
,`attribute1` int(4)
,`attribute2` int(4)
,`attribute3` int(4)
,`attribute4` int(4)
,`image` varchar(64)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `view_player_recipes`
--
DROP VIEW IF EXISTS `view_player_recipes`;
CREATE TABLE IF NOT EXISTS `view_player_recipes` (
`player_id` int(4)
,`ref_recipe_id` int(4)
,`ref_factory_id` int(4)
,`description` mediumtext
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `view_recipes`
--
DROP VIEW IF EXISTS `view_recipes`;
CREATE TABLE IF NOT EXISTS `view_recipes` (
`ref_recipe_id` int(4)
,`ref_factory_id` int(4)
,`description` mediumtext
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `view_resources`
--
DROP VIEW IF EXISTS `view_resources`;
CREATE TABLE IF NOT EXISTS `view_resources` (
`player_id` int(4)
,`ref_resource_id` int(4)
,`quantity` float
,`resource_name` varchar(32)
,`resource_value` float
,`icon` varchar(32)
,`description` mediumtext
,`material_name` varchar(32)
,`material_value` int(4)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `view_substrate_resource`
--
DROP VIEW IF EXISTS `view_substrate_resource`;
CREATE TABLE IF NOT EXISTS `view_substrate_resource` (
`ref_substrate_id` int(4)
,`ref_resource_id` int(4)
,`percent` float
,`oxide` float
,`resource_name` varchar(32)
,`substrate_name` varchar(32)
,`hardness` float
,`chemistry` varchar(128)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `view_tiles`
--
DROP VIEW IF EXISTS `view_tiles`;
CREATE TABLE IF NOT EXISTS `view_tiles` (
`tile_id` int(4)
,`owner_id` int(4)
,`x` int(4)
,`y` int(4)
,`z` int(4)
,`state` enum('new','excavating','empty','constructing','occupied','destructing')
,`ref_material_id` int(4)
,`substrate_remaining` int(4)
,`ref_factory_id` int(4)
,`factory_level` int(11)
,`created` datetime
,`material_name` varchar(32)
,`value` int(4)
,`username` varchar(32)
,`avatar_url` varchar(128)
,`factory_name` varchar(32)
,`description` varchar(512)
);
-- --------------------------------------------------------

--
-- Structure for view `view_chat`
--
DROP TABLE IF EXISTS `view_chat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`iometi5_caverns`@`localhost` SQL SECURITY DEFINER VIEW `view_chat` AS select `chat_buffer`.`ref_chat_id` AS `ref_chat_id`,`players`.`player_id` AS `player_id`,`players`.`username` AS `username`,`chat_buffer`.`text` AS `text`,`chat_buffer`.`timestamp` AS `timestamp` from (`players` join `chat_buffer`) where (`chat_buffer`.`player_id` = `players`.`player_id`);

-- --------------------------------------------------------

--
-- Structure for view `view_components`
--
DROP TABLE IF EXISTS `view_components`;

CREATE ALGORITHM=UNDEFINED DEFINER=`iometi5_caverns`@`localhost` SQL SECURITY DEFINER VIEW `view_components` AS select `components`.`component_id` AS `component_id`,`components`.`player_id` AS `player_id`,`components`.`ref_component_id` AS `ref_component_id`,`components`.`damage_taken` AS `damage_taken`,`components`.`dyn_attribute` AS `dyn_attribute`,`components`.`robot_id` AS `robot_id`,`ref_component`.`name` AS `name`,`ref_component`.`type` AS `type`,`ref_component`.`weight` AS `weight`,`ref_component`.`durability` AS `durability`,`ref_component`.`energy_drain` AS `energy_drain`,`ref_component`.`attribute1` AS `attribute1`,`ref_component`.`attribute2` AS `attribute2`,`ref_component`.`attribute3` AS `attribute3`,`ref_component`.`attribute4` AS `attribute4`,`ref_component`.`image` AS `image` from (`components` join `ref_component`) where (`components`.`ref_component_id` = `ref_component`.`ref_component_id`);

-- --------------------------------------------------------

--
-- Structure for view `view_player_recipes`
--
DROP TABLE IF EXISTS `view_player_recipes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`iometi5_caverns`@`localhost` SQL SECURITY DEFINER VIEW `view_player_recipes` AS select `players`.`player_id` AS `player_id`,`ref_recipe_process`.`ref_recipe_id` AS `ref_recipe_id`,`ref_recipe_process`.`ref_factory_id` AS `ref_factory_id`,`ref_recipe_process`.`description` AS `description` from ((`players` join `player_recipes`) join `ref_recipe_process`) where ((`players`.`player_id` = `player_recipes`.`player_id`) and (`player_recipes`.`ref_recipe_id` = `ref_recipe_process`.`ref_recipe_id`));

-- --------------------------------------------------------

--
-- Structure for view `view_recipes`
--
DROP TABLE IF EXISTS `view_recipes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`iometi5_caverns`@`localhost` SQL SECURITY DEFINER VIEW `view_recipes` AS select `ref_recipe_process`.`ref_recipe_id` AS `ref_recipe_id`,`ref_recipe_process`.`ref_factory_id` AS `ref_factory_id`,`ref_recipe_process`.`description` AS `description` from `ref_recipe_process`;

-- --------------------------------------------------------

--
-- Structure for view `view_resources`
--
DROP TABLE IF EXISTS `view_resources`;

CREATE ALGORITHM=UNDEFINED DEFINER=`iometi5_caverns`@`localhost` SQL SECURITY DEFINER VIEW `view_resources` AS select `resources`.`player_id` AS `player_id`,`resources`.`ref_resource_id` AS `ref_resource_id`,`resources`.`quantity` AS `quantity`,`ref_resource`.`name` AS `resource_name`,`ref_resource`.`value` AS `resource_value`,`ref_resource`.`icon` AS `icon`,`ref_resource`.`description` AS `description`,`ref_material`.`name` AS `material_name`,`ref_material`.`value` AS `material_value` from ((`resources` join `ref_resource`) join `ref_material`) where ((`resources`.`ref_resource_id` = `ref_resource`.`ref_resource_id`) and (`resources`.`ref_material_id` = `ref_material`.`ref_material_id`));

-- --------------------------------------------------------

--
-- Structure for view `view_substrate_resource`
--
DROP TABLE IF EXISTS `view_substrate_resource`;

CREATE ALGORITHM=UNDEFINED DEFINER=`iometi5_caverns`@`localhost` SQL SECURITY DEFINER VIEW `view_substrate_resource` AS select `ref_substrate_resource`.`ref_substrate_id` AS `ref_substrate_id`,`ref_substrate_resource`.`ref_resource_id` AS `ref_resource_id`,`ref_substrate_resource`.`percent` AS `percent`,`ref_substrate_resource`.`oxide` AS `oxide`,`ref_resource`.`name` AS `resource_name`,`ref_substrate`.`name` AS `substrate_name`,`ref_substrate`.`hardness` AS `hardness`,`ref_substrate`.`chemistry` AS `chemistry` from ((`ref_substrate_resource` join `ref_substrate`) join `ref_resource`) where ((`ref_substrate_resource`.`ref_substrate_id` = `ref_substrate`.`ref_substrate_id`) and (`ref_substrate_resource`.`ref_resource_id` = `ref_resource`.`ref_resource_id`));

-- --------------------------------------------------------

--
-- Structure for view `view_tiles`
--
DROP TABLE IF EXISTS `view_tiles`;

CREATE ALGORITHM=UNDEFINED DEFINER=`iometi5_caverns`@`localhost` SQL SECURITY DEFINER VIEW `view_tiles` AS select `tiles`.`tile_id` AS `tile_id`,`tiles`.`owner_id` AS `owner_id`,`tiles`.`x` AS `x`,`tiles`.`y` AS `y`,`tiles`.`z` AS `z`,`tiles`.`state` AS `state`,`tiles`.`ref_material_id` AS `ref_material_id`,`tiles`.`substrate_remaining` AS `substrate_remaining`,`tiles`.`ref_factory_id` AS `ref_factory_id`,`tiles`.`factory_level` AS `factory_level`,`tiles`.`created` AS `created`,`ref_material`.`name` AS `material_name`,`ref_material`.`value` AS `value`,`players`.`username` AS `username`,`players`.`avatar_url` AS `avatar_url`,`ref_factory`.`name` AS `factory_name`,`ref_factory`.`description` AS `description` from (((`tiles` join `ref_material`) join `ref_factory`) join `players`) where ((`tiles`.`owner_id` = `players`.`player_id`) and (`tiles`.`ref_material_id` = `ref_material`.`ref_material_id`) and ((`tiles`.`ref_factory_id` = `ref_factory`.`ref_factory_id`) or isnull(`tiles`.`ref_factory_id`)));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
