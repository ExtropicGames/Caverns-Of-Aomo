<?php

require_once ("includes/Authentication.php");
require_once ("functions-game.php");

session_start();

Authenticator::authenticate();

include "header.php";

Database::connect();

$username = $_SESSION['username'];

$player_id = Authenticator::getPlayerID();

$quantity = mysql_real_escape_string($_POST['quantity']);
$target_player = strtolower(mysql_real_escape_string($_POST['target_player']));
$resource_id = mysql_real_escape_string($_POST['resource_id']);
$material_id = mysql_real_escape_string($_POST['material_id']);

echo '<center>';

$player_result = mysql_query("SELECT * FROM players WHERE LOWER(username)='$target_player'");
if (mysql_num_rows($player_result) == 1) {
    $player_row = mysql_fetch_array($player_result);
    $inventory_row = mysql_fetch_array(mysql_query("SELECT * FROM view_resources WHERE player_id='$player_id' AND ref_resource_id='$resource_id' AND ref_material_id='$material_id'"));
    if ($inventory_row['quantity'] >= $quantity) {
        remove_resources($player_id, $resource_id, $material_id, $quantity);
        add_resources($player_row['player_id'], $resource_id, $material_id, $quantity);
        $body = "$username has given you $quantity ".$inventory_row['material_name']." ".$inventory_row['resource_name'].". Thanks, $username!";
        $subject = "Resources received";
        send_message($player_id, $player_row['player_id'], $subject, $body);
        echo "Resources given.";
    } else {
        echo "You don't have that many ".$inventory_row['material_name']." ".$inventory_row['resource_name'].".";
    }
} else {
    echo 'No player by that name was found.';
}

?>