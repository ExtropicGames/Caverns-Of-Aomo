<?php

require_once ("includes/Authentication.php");

session_start();

Authenticator::authenticate();

Database::connect();

$username = $_SESSION['username'];

$player_id = Authenticator::getPlayerID();

include("views/inbox-view.php");
?>