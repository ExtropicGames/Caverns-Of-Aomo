<?php
require_once ("includes/Authentication.php");

session_start();

Authenticator::authenticate();

if (!Authenticator::isAdmin()) {
    die('<meta http-equiv=refresh content="0;URL=index.php">');
}

include "views/header.php";

echo '<center>';
echo "select a player to view resources for:";
echo "<form name=lookup action=admin-resources.php method=post><input name=player_id><input type=submit value=Go><br></form>";

$player_id = mysql_real_escape_string($_POST['player_id']);
$resource = mysql_real_escape_string($_POST['resource']);
$amount = mysql_real_escape_string($_POST['amount']);
if (($resource != NULL or 0) && ($amount != NULL or 0)) {
    $query = "INSERT INTO resources (player_id, ref_resource_id, quantity) VALUES ('$player_id', '$resource', '$amount')";
    mysql_query($query);
}

if ($player_id != NULL) {
    $mysqli = Database::getConnection();
    echo "resources owned by $player_id<br>";
    $query = "SELECT * FROM view_resources WHERE player_id='$player_id'";
    $result = $mysqli->query($query);
    if ($result != NULL) {
        echo '<table class=spreadsheet>';
        echo '<tr>';
        echo '<td class=spreadsheet_alt>name</td>';
        echo '<td class=spreadsheet_alt>quantity</td>';
        echo '</tr>';
        while($row = $result->fetch_array()) {
            echo "<tr><td class=spreadsheet>" . $row['name'] . "</td><td class=spreadsheet>" . $row['quantity'] . "</td></tr>";
        }
        echo '</table>';
    }
    $result = $mysqli->query("SELECT * FROM ref_resource");
    if ($result != NULL) {
        echo "add resource to player $player_id's inventory:";
        echo '<form name=addresource action=admin-resources.php method=post>';
        echo "<input type=hidden name=player_id value=$player_id>";
        echo '<select name=resource>';
        while($row = $result->fetch_array()) {
            echo '<option value="' . $row['ref_resource_id'] . '">' . $row['name'] . '</option>';
        }
        echo '</select>';
        echo '<input name=amount>';
        echo '<input type=submit value=Add>';
        echo '</form>';
    }
}

echo '</center>';
?>