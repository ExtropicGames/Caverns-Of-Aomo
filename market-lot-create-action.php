<?php

require_once ("includes/Authentication.php");
require_once ('functions-game.php');

session_start();

Authenticator::authenticate();

include "header.php";

Database::connect();

$username = $_SESSION['username'];

$description = mysql_real_escape_string($_POST['description']);

$index = mysql_real_escape_string($_POST['index']);
$index = substr($index, 0, -1);
$array = explode(':', $index);

$x = 0;
foreach ($array as $i) {
    $temp = explode('-', $i);
    $item["$x"]['material_id'] = $temp[0];
    $item["$x"]['resource_id'] = $temp[1];
    $item["$x"]['quantity'] = mysql_real_escape_string($_POST["$i"]);
    $x++;
}

$player_id = Authenticator::getPlayerID();

include 'market-header.php';

$valid = true;
foreach ($item as $i) {
    $inventory_result = mysql_query("SELECT * FROM view_resources WHERE ref_material_id='".$i['material_id']."' AND ref_resource_id='".$i['resource_id']."' AND player_id='$player_id'");
    if (mysql_num_rows($inventory_result) == 1) {
        $inventory_row = mysql_fetch_array($inventory_result);
        if ($inventory_row['quantity'] < $i['quantity']) {
            $valid = false; echo "You don't have that many ".$inventory_row['material_name']." ".$inventory_row['resource_name']."s.";
        }
    } else {
        $valid = false; echo "Form error.";
    }
}

if ($valid == true) {
    mysql_query("INSERT INTO market_lots (player_id, description, time_posted, time_expire) VALUES ('$player_id', '$description', NOW(), NOW() + INTERVAL 1 WEEK)");
    echo "INSERT INTO market_lots (player_id, description, time_posted, time_expire) VALUES ($player_id, $description, NOW(), NOW() + INTERVAL 1 WEEK)";
    $lot_id = mysql_insert_id();
    foreach ($item as $i) {
        if ($i['quantity'] > 0) {
            remove_resources($player_id, $i['resource_id'], $i['material_id'], $i['quantity']);
            mysql_query("INSERT INTO market_items (lot_id, component_id, material_id, resource_id, quantity) VALUES ('$lot_id', NULL, '".$i['material_id']."', '".$i['resource_id']."', '".$i['quantity']."')");
        }
    }
}

echo "Lot #".$lot_id." successfully created.<br>";

?>