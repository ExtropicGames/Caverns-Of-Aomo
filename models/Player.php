<?php
class Player {

    // Vars
    private $player;

    private function __construct() {}

    // Pseudo-constructors

    public static function getPlayerByLogin($username, $password) {
        return Player::getPlayer("SELECT * FROM players WHERE username='$username' AND password='$password'");
    }

    public static function getPlayerByID($player_id) {
        return Player::getPlayer("SELECT * FROM players WHERE player_id='$player_id'");
    }

    public static function getAuthenticatedPlayer() {
        return Player::getPlayerByID(Authenticator::getPlayerID());
    }

    private static function getPlayer($query) {
        $mysqli = Database::getConnection();
        $result = $mysqli->query($query);
        if ($result == NULL) {
            throw new Exception('No results for player.');
        }
        if ($result->num_rows != 1) {
            throw new Exception('Expected one result for player ID, got ' . mysql_num_rows($result));
        }

        $return = new Player;
        $return->player = $result->fetch_assoc();
        return $return;
    }

    // Getters

    /**
     * @return int
     */
    function getPlayerID() {
        return $this->player['player_id'];
    }

    /**
     * @return string
     */
    function getUsername() {
        return $this->player['username'];
    }
}