<?php

require_once ("includes/Authentication.php");

session_start();

if (!Authenticator::authenticated()) {
    Authenticator::logout();
} else {
    include "header.php";
}

?>
<script type="text/javascript">
<!--
function doAction(robot_id, action, component_id, is_chassis) {
    document.robot.robotid.value = robot_id;
    switch (action) {
    case 1:
        document.robot.action.value = 'attach';
        break;
    case 2:
        document.robot.action.value = 'detach';
        break;
    default:
        document.robot.action.value = 'null';
    }
    document.robot.target.value = component_id;
    
    if (is_chassis == 1) {
        var chassis_confirm = confirm("Detaching the chassis will remove all components from the robot.")
        if (chassis_confirm) {
            document.robot.submit();
        } else {
            return;
        }
    } else {
        document.robot.submit();
    }
}
//-->
</script>
<?php

echo '<center>';

include ("robots-detail-action.php");

Database::connect();

$player_id = Authenticator::getPlayerID();

$robot_id = mysql_real_escape_string($_GET['robot-id']);
if ($robot_id == NULL) {
    die ("Invalid robot ID.");
}
    
$query = "SELECT * FROM robots WHERE player_id='$player_id' AND robot_id='$robot_id'";
$result = mysql_query($query);
if ($result != NULL) {
    echo '<form name="robot" action="' . $_SERVER['REQUEST_URI'] . '" method=post>';
    echo "<input type='hidden' name='robotid' value='$robot_id'><input type=hidden name=action><input type=hidden name=target>";
    $row = mysql_fetch_array($result);
    echo 'Name: ' . $row['name'] . '<br>';
    echo 'Status: ';
    switch($row['active_state']) {
    case 'working':
        $finish_time = strtotime($row['job_finish']);
        $start_time = strtotime($row['job_start']);
        $current_time = time();
        $seconds_elapsed = $current_time - $start_time;
        $seconds_total = $finish_time - $start_time;
        $percent_complete = floor(($seconds_elapsed / $seconds_total) * 100);
        echo "<font color=green><b>WORKING ($percent_complete% done)</b></font><br>";
        break;
    case 'idle':
        echo '<font color=yellow><b>IDLE</b></font><br>';
        break;
    case 'inactive':
        echo '<font color=red><b>INACTIVE</b></font><br>';
        break;
    default:
        echo '<font color=black><b>ERROR</b></font><br>';
    }
    echo 'Component list:<ul>';
    
    $query2 = "SELECT * FROM view_components WHERE player_id='$player_id' AND robot_id='$robot_id' ORDER BY type";
    $result2 = mysql_query($query2);
    if (mysql_num_rows($result2) != NULL) {
        while($row2 = mysql_fetch_array($result2)) {
            if ($row2['type'] == $CHASSIS) {
                echo '<b><li>' . $row2['name'] . " - <a href='javascript:;' onClick='doAction($robot_id , 2, " . $row2['component_id'] . ", 1)'>Detach</a></li></b>";
            } else {
                echo '<li>' . $row2['name'] . " - <a href='javascript:;' onClick='doAction($robot_id , 2, " . $row2['component_id'] . ", 0)'>Detach</a></li>";
            }
        }
    } else {
        echo '<li>No components</li>';
    }
    echo '</ul>';
    
    // Unattached components
    $query2 = "SELECT * FROM view_components WHERE player_id='$player_id' AND isnull(robot_id) ORDER BY type";
    $result2 = mysql_query($query2);
    if ($result2 != NULL) {
        echo "<hr>Unattached components:<br>";
        while($row2 = mysql_fetch_array($result2)) {
            echo $row2['name'] . " - <a href='javascript:;' onClick='doAction($robot_id, 1, " . $row2['component_id'] . ", 0)'>Attach</a><br>";
        }
    }
    echo '</form>';
} else {
    die ("Invalid robot ID.");
}
    
echo '</center>';
?>