<?php

function loadPostIntoSession($variableString, $default, $type='String') {
    if (isset($_POST[$variableString])) {
        if ($type == 'Integer') {
            $_SESSION[$variableString] = (int) mysql_real_escape_string($_POST[$variableString]);
        } else {  // default or unrecognized values are loaded as strings
            $_SESSION[$variableString] = mysql_real_escape_string($_POST[$variableString]);
            if ($type != 'String') {
                // print a warning about unrecognized type string
            }
        }
    } else {
        $_SESSION[$variableString] = $default;
    }
    
    return $_SESSION[$variableString];
}

function loadFromSession($variableString, $default) {
    if (isset($_SESSION[$variableString])) {
        return $_SESSION[$variableString];
    } else {
        $_SESSION[$variableString] = $default;
        return $default;
    }
}

function loadFromPost($variableString, $default, $type='String') {
    if (isset($_POST[$variableString])) {
        if ($type == 'Integer') {
            return (int) mysql_real_escape_string($_POST[$variableString]);
        } else {
            return mysql_real_escape_string($_POST[$variableString]);
        }
    } else {
        return $default;
    }
}

?>