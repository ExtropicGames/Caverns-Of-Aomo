<?php
require_once ("includes/Authentication.php");

session_start();

if (!Authenticator::isAdmin()) {
    die('<meta http-equiv=refresh content="0;URL=index.php">');
}

include "views/header.php";

echo '<center>';
echo "select a player to view materials for:";
echo "<form name=lookup action=admin-materials.php method=post><input name=player_id><input type=submit value=Go><br></form>";

$mysqli = Database::getConnection();

$player_id = mysql_real_escape_string($_POST['player_id']);
$material = mysql_real_escape_string($_POST['material']);
$amount = mysql_real_escape_string($_POST['amount']);
if (($material != NULL or 0) && ($amount != NULL or 0)) {
    $query = "INSERT INTO materials (player_id, materials_ref_id, quantity) VALUES ('$player_id', '$material', '$amount')";
    $mysqli->query($query);
}

if ($player_id != NULL) {
    echo "materials owned by $player_id<br>";
    $query = "SELECT * FROM view_materials WHERE player_id='$player_id'";
    $result = $mysqli->query($query);
    if ($result != NULL) {
        echo '<table class=spreadsheet>';
        echo '<tr>';
        echo '<td class=spreadsheet_alt>name</td>';
        echo '<td class=spreadsheet_alt>quantity</td>';
        echo '</tr>';
        while($row = $result->fetch_array()) {
            echo "<tr><td class=spreadsheet>" . $row['name'] . "</td><td class=spreadsheet>" . $row['quantity'] . "</td></tr>";
        }
        echo '</table>';
    }
    $result = $mysqli->query("SELECT * FROM ref_material");
    if ($result != NULL) {
        echo "add material to player $player_id's inventory:";
        echo '<form name=addmaterial action=admin-materials.php method=post>';
        echo "<input type=hidden name=player_id value=$player_id>";
        echo '<select name=material>';
        while($row = $result->fetch_array()) {
            echo '<option value="' . $row['ref_material_id'] . '">' . $row['name'] . '</option>';
        }
        echo '</select>';
        echo '<input name=amount>';
        echo '<input type=submit value=Add>';
        echo '</form>';
    }
}

echo '</center>';
?>