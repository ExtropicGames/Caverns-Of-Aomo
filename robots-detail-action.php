<?php

require_once("functions-game.php");

Database::connect();

$player_id = Authenticator::getPlayerID();

$action = mysql_real_escape_string($_POST['action']);
$target = mysql_real_escape_string($_POST['target']);

$fail = false;

if ($action == NULL) {
    echo ("Permission error: could not perform action.");
    die();
}

$robot_id = mysql_real_escape_string($_POST['robotid']);
if ($robot_id == NULL) {
    echo ("Invalid robot ID: could not attach.<br>"); $fail = true;
}

// If this returns a result, the robot is owned by the player.
$robot_result = mysql_query("SELECT * FROM robots WHERE player_id='$player_id' AND robot_id='$robot_id'");
if (mysql_num_rows($robot_result) == 1) {
    $robot_row = mysql_fetch_array($robot_result);
    if ($robot_row['active_state'] == 'working') {
        echo 'Cannot modify a working robot. '; $fail = true;
    }
        
    if (($action == 'attach') && ($target != NULL)) {
        // To attach a part:
        // 1. Check that the part and the robot are both owned by the player trying to attach them.
        // 2. Check that the part isn't attached to another robot
        // 3. Check that the robot has an open slot to attach the part to.
        // 4. Attach the part.
            
        // If this query is successful, the part was owned by the player and the part isn't attached to another robot.
		$result2 = mysql_query("SELECT * FROM components WHERE robot_id IS NULL AND player_id='$player_id'");
		if (!$result2) {
            echo ("Component permission error: Could not attach."); $fail = true;
        }
           
		// Get the part type of the part to be attached
        $get_target_part_type_result = mysql_query("SELECT type FROM view_components WHERE component_id='$target' AND player_id='$player_id'");
        $row = mysql_fetch_array($get_target_part_type_result);
        $part_type = $row['type'];
			
		// Get the chassis ID
        $get_chassis_id_result = mysql_query("SELECT attribute1 FROM view_components WHERE robot_id='$robot_id' AND player_id='$player_id' AND type='chassis'");
		// if there is no chassis attached and the player is trying to attach something other than a chassis...
		if ((mysql_num_rows($get_chassis_id_result) == 0) && ($part_type != 'chassis')) {
            echo "The robot must have a chassis in order to attach components."; $fail = true;
        }
        if ((mysql_num_rows($get_chassis_id_result) == 1) && ($part_type == 'chassis')) {
            echo "Only one chassis can be attached at a time."; $fail = true;
        }
		// Now get the chassis type ID and look up the number of each type of part that can be attached.
        $row = mysql_fetch_array($get_chassis_id_result);
        $chassis_id = $row['attribute1'];
        $max_attached_parts_result = mysql_query("SELECT * FROM ref_chassis WHERE ref_chassis_id='$chassis_id'");
        $row = mysql_fetch_array($max_attached_parts_result);
        $max_arms = $row['num_arms'];
        $max_legs = $row['num_legs'];
        $max_heads = $row['num_heads'];
        $max_power_supplies = $row['num_power_supplies'];
			
		// Now count how many of each part type is already attached.
        $get_current_parts_result = mysql_query("SELECT type, COUNT(*) FROM view_components WHERE robot_id='$robot_id' AND player_id='$player_id' GROUP BY type");
        while ($row = mysql_fetch_array($get_current_parts_result)) {
            if ($row['type'] == $part_type) {
                if (($part_type == 'arm') && ($row['COUNT(*)'] >= $max_arms)) {
                    echo 'Too many arms are already attached.'; $fail = true;
                }
                if (($part_type == 'leg') && ($row['COUNT(*)'] >= $max_legs)) {
                    echo 'Too many legs are already attached.'; $fail = true;
                }
                if (($part_type == 'head') && ($row['COUNT(*)'] >= $max_heads)) {
                    echo 'Too many heads are already attached.'; $fail = true;
                }
                if (($part_type == 'power supply') && ($row['COUNT(*)'] >= $max_power_supplies)) {
                    echo 'Too many power supplies are already attached.'; $fail = true;
                }
            }
        }
        
		// If you get to this point, you get to attach the part! Yay!
        if ($fail == false) {
            $attach_result = mysql_query("UPDATE components SET robot_id='$robot_id' WHERE component_id='$target' AND player_id='$player_id'");
            if (!$attach_result) {
                echo mysql_error();
                die("Unspecified database error.");
            } else {
                check_active_status($player_id, $robot_id);
            }
        }
	}
	// It is much easier to detach a component than it is to attach one.
    if (($action == 'detach') && ($target != NULL)) {
        $detach_result = mysql_query("SELECT type FROM view_components WHERE component_id='$target' AND player_id='$player_id' AND robot_id='$robot_id'");
        if (mysql_num_rows($detach_result) != 1) {
            echo "Permission error: could not detach.";
        } else {
            if ($fail == false) {
                $row = mysql_fetch_array($detach_result);
                // If the part the player is trying to detach is a chassis, detach all parts from the robot.
                if ($row['type'] == 'chassis') {
                    mysql_query("UPDATE components SET robot_id=NULL WHERE player_id='$player_id' AND robot_id='$robot_id'");
                } else {
                    mysql_query("UPDATE components SET robot_id=NULL WHERE component_id='$target' AND player_id='$player_id' ANd robot_id='$robot_id'");
                }
                check_active_status($player_id, $robot_id);
            }
        }
    }
}
       
?>