<?php

require_once ("includes/Authentication.php");
include("models/Player.php");

session_start();

Authenticator::authenticate();

if (isset($_POST['chattext'])) {
    $mysqli = Database::getConnection();
    $player_id = Authenticator::getPlayerID();
    $text = strip_tags($mysqli->escape_string($_POST['chattext']));
    $mysqli->query("INSERT INTO chat_buffer(ref_chat_id, player_id, text, timestamp) VALUES (NULL, '$player_id', '$text', CURRENT_TIMESTAMP)");
}

$player = Player::getPlayerByID(Authenticator::getPlayerID());

include "views/header.php";

?>