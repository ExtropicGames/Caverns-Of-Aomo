<?php

require_once ("includes/Authentication.php");

session_start();

Authenticator::authenticate();

include "header.php";

Database::connect();

$username = $_SESSION['username'];

$player_id = Authenticator::getPlayerID();
$lot_id = mysql_real_escape_string($_POST['lot_id']);

include 'market-header.php';

$lot_row = mysql_fetch_array(mysql_query("SELECT * FROM market_lots WHERE lot_id='$lot_id'"));

if ($player_id == $lot_row['player_id']) {
    echo "You cannot bid on your own lot!";
    die();
}

// Resources list
$query = "SELECT * FROM view_resources WHERE player_id='$player_id'";
$result = mysql_query($query);
if ($result != NULL) {
    echo '<form action="market-offer-create-action.php" method=post>';
    echo 'Message to send with offer:<br><textarea name=description cols=30 rows=10></textarea><br>';
    echo '<br><br>Choose items to offer:<br><br>';
    
    $x = 0;
    $index = "";
    echo '<table>';
    echo '<tr>';
    while($row = mysql_fetch_array($result)) {
        echo '<td>';
        echo '<input type=text size=1 name='.$row['ref_material_id'].'-'.$row['ref_resource_id'].' value=0>/'.$row['quantity'];
        $index = $index . $row['ref_material_id'] . '-' . $row['ref_resource_id'] . ':';
        echo '  <img src="images/resources/' . $row['material_name'].'-'.$row['resource_name'].'.png">  '.$row['material_name'] . ' ' . $row['resource_name'];
        echo '</td>';
        $x++;
        if ($x > $ROWSIZE) {
            echo '</tr><tr>';
            $x = 0;
        }
    }
    echo '</table>';
    echo "<input type=hidden name='index' value='$index'>";
    echo "<input type=hidden name='lot_id' value='$lot_id'>";
    echo "<input type=submit value='Create Offer'>";
    echo '</form>';
} else {
    echo 'You have no resources to offer.';
}

?>