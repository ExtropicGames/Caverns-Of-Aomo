<?php

require_once ("includes/Authentication.php");

session_start();

Authenticator::authenticate();

if (!isset($_SESSION['homemap_x'])) {
    $_SESSION['homemap_x'] = 0;
}
if (!isset($_SESSION['homemap_y'])) {
    $_SESSION['homemap_y'] = 0;
}
if (!isset($_SESSION['homemap_z'])) {
    $_SESSION['homemap_z'] = 0;
}

$x = $_SESSION['homemap_x'];
$y = $_SESSION['homemap_y'];
$z = $_SESSION['homemap_z'];
    
if (isset($_POST['move_x'])) {
    $x = (int)mysql_real_escape_string($_POST['move_x']); 
    $_SESSION['homemap_x'] = $x;
}

if (isset($_POST['move_y'])) {
    $y = (int)mysql_real_escape_string($_POST['move_y']); 
    $_SESSION['homemap_y'] = $y;
}

if (isset($_POST['move_z'])) {
    $z = (int)mysql_real_escape_string($_POST['move_z']); 
    $_SESSION['homemap_z'] = $z;
}

$mysqli = Database::getConnection();

$query = "SELECT map_id FROM player WHERE username='$username'";
$result = $mysqli->query($query);

if ($result->num_rows == 0) {
    die('Could not find a matching map_id in the database.');
}

$i = $result->fetch_array();

$map_id = $i['map_id'];

$query = "SELECT * FROM tiles WHERE map_id=$map_id AND (x=$x OR x=$x + 1 OR x=$x - 1) AND (y=$y OR y=$y+1 OR y=$y-1) ORDER BY x DESC, y ASC";
$result = $mysqli->query($query);
if ($result == NULL) {
    die("The system is down. (Error code = 7) Please contact your administrator.");
}
    
while($row = mysql_fetch_array($result)) {
    $map[$row['x']][$row['y']]['state'] = $row['state'];
    $map[$row['x']][$row['y']]['substrate'] = $row['substrate_ref_id'];
    $map[$row['x']][$row['y']]['substrate_remaining'] = $row['substrate_remaining'];
}

include('views/home-view.php');