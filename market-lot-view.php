<?php

require_once ("includes/Authentication.php");

session_start();

Authenticator::authenticate();

include "header.php";

Database::connect();

$username = $_SESSION['username'];

$player_id = Authenticator::getPlayerID();

$lot_id = mysql_real_escape_string($_GET['lot_id']);

// Market header
include 'market-header.php';

echo '<center>';

$lot_row = mysql_fetch_array(mysql_query("SELECT * FROM market_lots WHERE lot_id='$lot_id'"));
echo "Viewing lot #$lot_id owned by ".$lot_row['player_id']."<br>";
echo "Lot expires at ".$lot_row['time_expire'].".<br>";

$item_result = mysql_query("SELECT * FROM market_items WHERE lot_id='$lot_id'");
if (mysql_num_rows($item_result) > 0) {
    while ($item_row = mysql_fetch_array($item_result) ) {
        // take these excess queries out once the view is implemented
        $material_row = mysql_fetch_array(mysql_query("SELECT * FROM ref_material WHERE ref_material_id=".$item_row['material_id'].""));
        $resource_row = mysql_fetch_array(mysql_query("SELECT * FROM ref_resource WHERE ref_resource_id=".$item_row['resource_id'].""));
        echo $material_row['name'].' '.$resource_row['name'].' - '.$item_row['quantity'].'<br>';
    }
    
    echo '<br>';
    echo '<form name=offer action=market-offer-create.php method=post>';
    echo "<input type=hidden name=lot_id value=$lot_id>";
    echo '<input type=submit value="Place offer on lot">';
    echo '</form>';
} else {
    echo 'Invalid lot ID.';
}
?>
