<?php

// Robots information
class RobotParts {
    const Chassis = 1;
    const PowerSupply = 2;
    const Arm = 3;
    const Leg = 4;
    const Head = 5;
}

// Robot arm types
class ArmTypes {
    const DIGGING = 1;
    const MANUFACTURING = 2;
    const RESEARCHING = 3;
    const CONSTRUCTING = 4;
}

?>
