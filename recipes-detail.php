<?php

require_once ("includes/Authentication.php");

session_start();

if (!Authenticator::authenticated()) {
    Authenticator::logout();
} else {
    include "header.php";
}

Database::connect();

$username = $_SESSION['username'];

$player_id = Authenticator::getPlayerID();

$recipe_id = mysql_real_escape_string($_GET['recipe_id']);
echo '<center>';

$recipe_result = mysql_query("SELECT * FROM view_player_recipes WHERE player_id='$player_id' AND ref_recipe_id='$recipe_id'");
if (($recipe_result != NULL) && (mysql_num_rows($recipe_result) > 0)) {
    $recipe_row = mysql_fetch_array($recipe_result);
    $input_result = mysql_query("SELECT * FROM ref_recipe_input WHERE ref_recipe_id='$recipe_id'");
    echo '<b>required resources:</b><br>';
    while ($input_row = mysql_fetch_array($input_result)) {
        $resource_result = mysql_query("SELECT * FROM ref_resource WHERE ref_resource_id='".$input_row['input_resource_id']."'");
        $resource_row = mysql_fetch_array($resource_result);
        $material_result = mysql_query("SELECT * FROM ref_material WHERE ref_material_id='".$input_row['input_material_id']."'");
        $material_row = mysql_fetch_array($material_result);
        echo $material_row['name'].' '.$resource_row['name'].': '.$input_row['input_quantity'].'<br>';
    }
    echo '<b>items produced:</b><br>';
    $output_result = mysql_query("SELECT * FROM ref_recipe_output WHERE ref_recipe_id='$recipe_id'");
    while ($output_row = mysql_fetch_array($output_result)) {
        if ($output_row['output_component_id'] != NULL) {
            $component_result = mysql_query("SELECT * FROM ref_component WHERE ref_component_id='".$output_row['output_component_id']."'");
            $component_row = mysql_fetch_array($component_result);
            echo $component_row['name'].': '.$output_row['output_quantity'].'<br>';
        } else {
            if ($output_row['output_resource_id'] != NULL) {
                $resource_result = mysql_query("SELECT * FROM ref_resource WHERE ref_resource_id='".$output_row['output_resource_id']."'");
                $resource_row = mysql_fetch_array($resource_result);
                $material_result = mysql_query("SELECT * FROM ref_material WHERE ref_material_id='".$output_row['output_material_id']."'");
                $material_row = mysql_fetch_array($material_result);
                echo $material_row['name'].' '.$resource_row['name'].': '.$output_row['output_quantity'].'<br>';
            } else {
                echo 'recipe error: incorrect output';
            }
        }
    }
} else {
    echo 'recipe not found';
}

?>