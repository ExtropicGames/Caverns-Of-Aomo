<?php include("header.php"); ?>

<!--suppress HtmlUnknownTarget -->

<script type="text/javascript">
    function move(x, y, z) {
        document.world.map_x.value = x;
        document.world.map_y.value = y;
        document.world.map_z.value = z;
        document.world.submit();
    }

    function view(x, y) {
        document.world.target_x.value = x;
        document.world.target_y.value = y;
        document.world.submit();
    }

    function doAction(action, robot_id) {
        if (action == 1) {
            document.mining.robot_id.value = robot_id;
            document.mining.action.value = 'mining';
            document.mining.submit();
        }
    }
}
</script>

