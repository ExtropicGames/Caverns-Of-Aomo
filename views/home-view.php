<?php include "header.php"; ?>

<!--suppress HtmlUnknownTarget -->

<script type="text/javascript">
    function move(x, y, z) {
        document.map.move_x.value = x;
        document.map.move_y.value = y;
        document.map.move_z.value = z;
        document.map.submit();
    }
</script>

<center><form name="map" action="home.php" method=post>
    <input type=hidden name=move_x>
    <input type=hidden name=move_y>
    <input type=hidden name=move_z>
    <table>
        <?php for ($i = ($y + 1); $i >= ($y - 1); $i--) { ?>
            <tr>
                <?php for ($m = ($x - 1); $m <= ($x + 1); $m++) { ?>
                    <td>
                        <?php if ($map[$m][$i]['state'] == 'new') { ?>
                            <a href='javascript:;' onClick='move($m, $i, 0)'><img src='images/rock.png'></a>
                        <?php } else if ($map[$m][$i]['state'] == 'occupied') { ?>
                            <a href='javascript:;' onClick='move($m, $i, 0)'><img src='images/city.png'></a>
                        <?php } else if ($map[$m][$i]['state'] == 'empty') { ?>
                            <a href='javascript:;' onClick='move($m, $i, 0)'><img src='images/empty.png'></a>
                        <?php } else { ?>
                            <img src="images/unexplored.png">
                        <?php } ?>
                    </td>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>

    <?php if ($map[$x][$y]['state'] == 'new') { ?>
        <?php
            //! \todo Move this to the data model.
            $query = "SELECT * FROM substrate_ref WHERE substrate_ref_id='" . $map[$x][$y]['substrate'] . "'";
            $i = mysql_fetch_array(mysql_query($query));
        ?>
        This is an outcropping of <?php echo $i['name']; ?>.<br>
        <?php echo $i['description']; ?><br>
        <?php echo $map[$x][$y]['substrate_remaining']; ?> kg of material is remaining.<br>
    <?php } ?>

    <?php if ($map[$x][$y]['state'] == 'empty') { ?>
        <?php
            //! \todo Move this to the data model.
            $query = "SELECT * FROM substrate_ref WHERE substrate_ref_id='" . $map[$x][$y]['substrate'] . "'";
            $i = mysql_fetch_array(mysql_query($query));
        ?>
        This is an empty room. The walls and floors are hewn from <?php echo $i['name']; ?>.<br>
    <?php } ?>

</form></center>