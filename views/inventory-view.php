<?php include('header.php'); ?>

<!--suppress HtmlUnknownTarget -->

<link rel="stylesheet" href="style/tooltip.css">
<script type="text/javascript" src="script/tooltip.js"></script>

<center>
    Resources:<br><br><br><br>

    <table>
        <tr>
            <?php foreach ($resources as $r) { ?>
                <td>
                    <a href="inventory-detail.php?resource=<?php echo $r['ref_resource_id']; ?>&material=<?php echo $r['ref_material_id']; ?>">
                        <img onmouseover="tooltip.show('<u> <?php echo $r['material_name']; ?> <?php echo $r['resource_name']; ?> </u><br> <?php echo $r['description']; ?>');" onmouseout="tooltip.hide();" src="images/resources/<?php echo $r['material_name']; ?>-<?php echo $r['resource_name']; ?>.png">
                        <br>
                        <?php echo $r['quantity']; ?>
                    </a>
                </td>
            <?php } ?>
        </tr>
    </table>