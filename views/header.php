<?php include('controllers/master.php'); ?>
<!DOCTYPE html>

<html lang='en'>

<head>
    <title>Caverns of Aomo</title>
    <link rel=stylesheet type=text/css href='style/default.css'>
    <link href='css/bootstrap.min.css' rel='stylesheet' media='screen'>
</head>

<body>
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <ul class='nav nav-pills'>
        <li><a href='inventory.php'>Inventory</a></li>
        <li><a href='robots-overview.php'>Robots</a></li>
        <li><a href='world.php'>World Map</a></li>
        <li><a href='hub.php'>Hub</a></li>
        <li><a href='inbox.php'>Log Out</a></li>
        <?php if (Authenticator::isAdmin()) : ?>
        <li><a href='admin-components.php'>Edit Components</a></li>
        <li><a href='admin-inventory.php'>Edit Inventory</a></li>
        <li><a href='admin-materials.php'>Edit Materials</a></li>
        <li><a href='admin-players.php'>Edit Players</a></li>
        <li><a href='admin-resources.php'>Edit Resources</a></li>
        <li><a href='admin-robots.php'>Edit Robots</a></li>
        <li><a href='admin-world.php'>Edit World</a></li>
        <?php endif; ?>
    </ul>
    <br>
    <div class='offset3 span6'>
        Logged in as: <?php echo $player->getUsername(); ?><br><br>
    </div>
    <div class='well span3'>
        <br><br>
        <?php
            //! \todo Move this to the data model.
            $mysqli = Database::getConnection();
            $query = "SELECT * FROM players WHERE username='" . $player->getUsername() . "'";
            $player_row = $mysqli->query($query)->fetch_array();
            $result = $mysqli->query("SELECT * FROM (SELECT * FROM view_chat ORDER BY ref_chat_id DESC LIMIT ".$player_row['settings_chatbox_length'].") AS chatview ORDER BY ref_chat_id ASC");
        ?>
        <?php while ($row = $result->fetch_array()) : ?>
            <?php
                date_default_timezone_set('UTC');
                $time = new DateTime($row['timestamp']);
            ?>
            <strong><?php echo $row['username']; ?></strong> <em>(<?php echo $time->format('n-j, G:i'); ?>)</em>: <?php echo $row['text']; ?><br>
        <?php endwhile; ?>
        <form action=index.php method=post>

            <input type='text' name='chattext' placeholder='Type here to chat...'>
        </form>
    </div>