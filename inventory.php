<?php

require_once ("includes/Authentication.php");
include('models/Player.php');

session_start();

Authenticator::authenticate();

$player = Player::getPlayerByID(Authenticator::getPlayerID());

$ROWSIZE = 3;

$mysqli = Database::getConnection();
$query = "SELECT * FROM view_resources WHERE player_id='" . Authenticator::getPlayerID() . "'";
$result = $mysqli->query($query);

if ($mysqli->errno) {
    throw new Exception($mysqli->error);
}

if (!$result) {
    $resources = NULL;
} else {
    $resources = $result->fetch_all();
}

include('views/inventory-view.php');

?>