<?php
	include ("config.php");

	if (session_id() == "") {
        session_start();
    }
    
    if (!isset ($_POST['agree'])) {
        echo "You must agree to the terms of the EULA before proceeding.<br>";
        echo "<a href='eula.php'>Back</a>";
        die();
    }
?>

<html>
    <head>
        <link rel=stylesheet type=text/css href=<?php echo SystemInfo::StylesheetURL ?>>
        <title><?php echo SystemInfo::PageTitle; echo SystemInfo::Version;?></title>
    </head>
    <body>
		<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
		
		<h3>Register for a new account</h3>
		<form action="register-validate.php" method=post>
			<font color=red>*</font> Username: <input maxlength=32 size=18 name=username><br><br>
			<font color=red>*</font> Password: <input maxlength=32 size=18 type=password name=password><br>
            <font color=red>*</font> Confirm Password: <input maxlength=32 size=18 type=password name=passwordconfirm><br><br>
            First Name: <input maxlength=32 size=18 name=firstname><br><br>
            Last Name: <input maxlength=32 size=18 name=lastname><br><br>
            <font color=red>*</font> Email Address: <input maxlength=32 size=18 name=email><br><br>
            Gender: <input type=radio name=gender value="Male">Male <input type=radio name=gender value="Female">Female<br><br>
            <font color=red>*</font> Birthday: <input maxlength=2 size=2 name=month>/<input maxlength=2 size=2 name=day>/<input maxlength=4 size=4 name=year><br><br>
            Fields with a <font color=red>*</font> are required.<br>
            Password must be at least 5 characters.<br><br>
			<input type=submit value=Register>
		</form>
	</body>
</html>