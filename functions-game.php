<?php
require_once ("includes/Database.php");

function adjust_player_resources($ref_resource_id, $player_id, $quantity) {
    $mysqli = Database::getConnection();
    
    $result = $mysqli->query("SELECT * FROM resources WHERE ref_resource_id='$ref_resource_id' AND player_id='$player_id'");
    if ($result == NULL) {
        $mysqli->query("INSERT INTO resources (player_id, ref_resource_id, quantity) VALUES ('$player_id', '$ref_resource_id', '$quantity')");
    } else {
        $mysqli->query("UPDATE resources SET quantity = (quantity+'$quantity') WHERE ref_resource_id='$ref_resource_id' AND player_id='$player_id'");
    }
}

function add_resources($player_id, $ref_resource_id, $ref_material_id, $quantity) {
    if ($quantity < 0) {
        remove_resources($player_id, $ref_resource_id, $ref_material_id, abs($quantity));
    }
    
    $mysqli = Database::getConnection();
    $mysqli->query("INSERT INTO resources (player_id, ref_resource_id, ref_material_id, quantity) VALUES ('$player_id', '$ref_resource_id', '$ref_material_id', '$quantity') ON DUPLICATE KEY UPDATE quantity=quantity+'$quantity'");
}

function remove_resources($player_id, $ref_resource_id, $ref_material_id, $quantity) {
    $quantity = abs($quantity);
    $mysqli = Database::getConnection();
    $resource_result = $mysqli->query("SELECT * FROM resources WHERE player_id='$player_id' AND ref_resource_id='$ref_resource_id' AND ref_material_id='$ref_material_id'");
    if ($resource_result->num_rows < 1) {
        return;
    }
    $resource_row = $resource_result->fetch_array();
    if (($resource_row['quantity'] - $quantity) <= 0) {
        $mysqli->query("DELETE FROM resources WHERE player_id='$player_id' AND ref_resource_id='$ref_resource_id' AND ref_material_id='$ref_material_id'");
    } else {
        $mysqli->query("UPDATE resources SET quantity=(quantity-'$quantity') WHERE player_id='$player_id' AND ref_resource_id='$ref_resource_id' AND ref_material_id='$ref_material_id'");
    }
}

function send_message($from, $to, $subject, $body) {
    mysql_query("INSERT INTO messages (from_player_id, to_player_id, subject, body, date) VALUES('$from', '$to', '$subject', '$body', NOW())");
}

function get_idle_robots($player_id, $component_criteria) {
    $mysqli = Database::getConnection();

    if ($component_criteria != NULL) {
        $component_result = $mysqli->query("SELECT robot_id FROM view_components WHERE player_id='$player_id' AND " . $component_criteria . " AND robot_id IS NOT NULL GROUP BY robot_id");
        while ($component_row = $component_result->fetch_array()) {
            $robot_result = $mysqli->query("SELECT * FROM robots WHERE robot_id='".$component_row['robot_id']."' AND player_id='$player_id' AND active_state='idle' AND job_tile IS NULL");
            if ($robot_result->num_rows != 0) {
                while ($robot_row = $robot_result->fetch_array()) {
                    $idle_robots[] = $robot_row;
                }
            }
        }
    } else {
        $robot_result = $mysqli->query("SELECT * FROM robots WHERE player_id='$player_id' AND active_state='idle' AND job_tile IS NULL");
        if ($robot_result->num_rows != 0) {
            while ($robot_row = $robot_result->fetch_array()) {
                $idle_robots[] = $robot_row;
            }
        }
    }
    return $idle_robots;
}

function update_robot_jobs($robot_id) {
    $mysqli = Database::getConnection();
    $result = $mysqli->query("SELECT * FROM robots WHERE robot_id='$robot_id'");
    $robot = $result->fetch_array();
    // if the current time is past the finish time, the job is finished
    $finish_time = strtotime($robot['job_finish']);
    $start_time = strtotime($robot['job_start']);
    $current_time = time();
    if ($current_time < $start_time) {
        echo 'time error';
    }
    if ($current_time >= $finish_time) {
        complete_job($robot);
    } else {
        $time['seconds_elapsed'] = $current_time - $start_time;
        $time['seconds_remaining'] = $finish_time - $current_time;
        $time['seconds_total'] = $finish_time - $start_time;
        $time['percent_complete'] = floor(($time['seconds_elapsed'] / $time['seconds_total']) * 100);
        $time['percent_remaining'] = 100 - $time['percent_complete'];
        return $time;
    }

}

function update_tile_jobs($tile_id) {
    $mysqli = Database::getConnection();
    $result = $mysqli->query("SELECT * FROM robots WHERE job_tile='$tile_id' ORDER BY job_start ASC");
    while ($robot = $result->fetch_array()) {
        $finish_time = strtotime($robot['job_finish']);
        $start_time = strtotime($robot['job_start']);
        $current_time = time();
        if ($current_time >= $finish_time) {
            complete_job($robot);
        } else {
            $time['seconds_elapsed'] = $current_time - $start_time;
            $time['seconds_remaining'] = $finish_time - $current_time;
            $time['seconds_total'] = $finish_time - $start_time;
            $time['percent_complete'] = floor(($time['seconds_elapsed'] / $time['seconds_total']) * 100);
            $time['percent_remaining'] = 100 - $time['percent_complete'];
        }
    }
}

function complete_job($robot) {
    include("config.php");
    require_once("includes/Authentication.php");
    $mysqli = Database::getConnection();
    $player_id = Authenticator::getPlayerID();
    
    switch($robot['job_type']) {
    case 'mining':
        $tile_row = $mysqli->query("SELECT * FROM tiles WHERE tile_id='" . $robot['job_tile'] . "'")->fetch_array();
        $digging_row = $mysqli->query("SELECT SUM(attribute2) FROM view_components WHERE robot_id='" . $robot['robot_id'] . "' AND type='arm' AND attribute1='" . ArmTypes::DIGGING . "' GROUP BY robot_id")->fetch_array();
        $material_row = $mysqli->query("SELECT * FROM ref_material WHERE ref_material_id='".$tile_row['ref_material_id']."'")->fetch_array();
        $digging_capacity = $digging_row['SUM(attribute2)'];
        $substrate_remaining = $tile_row['substrate_remaining'];
        $resource_mined = $material_row['default_resource_id'];
        // If the tile is completely empty...
        if ($substrate_remaining <= $digging_capacity) {
            add_resources($robot['player_id'], $resource_mined, $tile_row['ref_material_id'], $substrate_remaining);
            $mysqli->query("UPDATE robots SET job_tile=NULL, job_start=NULL, job_finish=NULL, job_type=NULL, active_state='idle' WHERE robot_id='".$robot['robot_id']."'");
            $mysqli->query("UPDATE tiles SET substrate_remaining=0, state='empty' WHERE tile_id='".$tile_row['tile_id']."'");
            $new_tile_result = $mysqli->query("SELECT * FROM tiles WHERE (x<=('".$tile_row['x']."' + 1) OR x>=('".$tile_row['x']."' - 1)) AND (y<=('".$tile_row['y']."' + 1) OR y>=('".$tile_row['y']."' - 1)) ORDER BY x DESC, y ASC");
            while ($row = $new_tile_result->fetch_array()) {
                $new_tile_array[$row['x']][$row['y']]['state'] = $row['state'];
            }
            for ($i = ($tile_row['y'] + 1); $i >= ($tile_row['y'] - 1); $i--) {
                for ($m = ($tile_row['x'] - 1); $m <= ($tile_row['x'] + 1); $m++) {
                    if (!isset($new_tile_array[$m][$i]['state'])) {
                        $mysqli->query("INSERT INTO tiles (x, y, z, state, ref_material_id, substrate_remaining, created) VALUES($m, $i, 0, 'new', 1, 25, NOW())");
                    }
                }
            }
        } else {
            $mysqli->query("UPDATE tiles SET substrate_remaining='".($substrate_remaining - $digging_capacity)."' WHERE tile_id='".$tile_row['tile_id']."'");
            add_resources($robot['player_id'], $resource_mined, $tile_row['ref_material_id'], $digging_capacity);
            $mysqli->query("UPDATE robots SET job_tile=NULL, job_start=NULL, job_finish=NULL, job_type=NULL, active_state='idle' WHERE robot_id='".$robot['robot_id']."'");
        }
        break;
    case 'manufacturing':
        $job_result = $mysqli->query("SELECT * FROM robots WHERE robot_id='".$robot['robot_id']."'");
        $job_row = $job_result->fetch_array();
        $recipe_result = $mysqli->query("SELECT * FROM ref_recipe_output WHERE ref_recipe_id='".$job_row['job_recipe']."'");
        while ($recipe_row = $recipe_result->fetch_array()) {
            if ($recipe_row['output_component_id'] != NULL) {
                for ($x = 1; $x <= $recipe_row['output_quantity']; $x++) {
                    $mysqli->query("INSERT INTO components (player_id, ref_component_id, damage_taken, dyn_attribute, created, robot_id) VALUES ('".$robot['player_id']."', '".$recipe_row['output_component_id']."', '0', 0, NOW(), 0)");
                }
            } else {
                add_resources($robot['player_id'], $recipe_row['output_resource_id'], $recipe_row['output_material_id'], $recipe_row['output_quantity']);
            }
        }
        $mysqli->query("UPDATE robots SET job_tile=NULL, job_start=NULL, job_finish=NULL, job_type=NULL, job_factory=NULL, job_recipe=NULL, active_state='idle' WHERE robot_id='".$robot['robot_id']."'");
        break;
    }
}

function check_recipe($player_id, $recipe_id) {
    return;
}

function check_active_status($player_id, $robot_id) {
    include "config.php";
    $mysqli = Database::getConnection();
    
    $activate_result = $mysqli->query("SELECT * FROM view_components WHERE player_id='$player_id' AND robot_id='$robot_id'");
    $has_chassis = false; $has_head = false; $has_power = false; $has_arm = false; $has_legs = false; $has_processor = false;
    while ($row = $activate_result->fetch_array()) {
        if ($row['type'] == 'chassis') {
            $has_chassis = true;
        } else if ($row['type'] == 'head') {
            $has_head = true;
        } else if ($row['type'] == 'power supply') {
            $has_power = true;
        } else if ($row['type'] == 'arm') {
            $has_arm = true;
        } else if ($row['type'] == 'leg') {
            $has_legs = true;
        }
    }
    if (($has_chassis == true) && ($has_head == true) && ($has_power == true) && ($has_arm == true) && ($has_legs == true)) {
        $mysqli->query("UPDATE robots SET is_active=1, active_state='idle' WHERE player_id='$player_id' AND robot_id='$robot_id'");
    } else {
        $mysqli->query("UPDATE robots SET is_active=0, active_state='inactive' WHERE player_id='$player_id' AND robot_id='$robot_id'");
    }
}

?>